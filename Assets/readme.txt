Mutable Character Customization for Unity
----------------------------------------------

This package includes all you need to create your own character customization in your Unity 4 game or application.
Mutable is a powerful and flexible system that lets you create your own customizable objects and let your playes build them in real-time in your game. It requires Unity 4 Professional, since it uses native plugins. It currently supports Windows, OSX and Linux, all for 32-bits and 64-bits games. It also supports iOS devices.
More information is available at our wiki http://support.anticto.com/projects/mutable-unity-4-integration/wiki

The content creation tool is available also for Windows (32 and 64 bit), and for OSX (64 bit only). More platforms will be added in future releases.

Contents
--------

- the Mutable Tool for Windows. Use this tool to import your data and generate the customizable objects for your game.
- Mutable Runtime plugin for Windows, OSX, Linux and iOS.
- Integration scripts to use the customizable objects in Unity
- Example models of a full skinned character with several customization options and a rigid helmet.
- Example scenes showing how to set up the customization.

Using the stand-alone tool
--------------------------

The Mutable plugin uses data exported from the Mutable Tool. This tool is a stand-alone tool and it is included in the package.
In order to launch this tool:
Windows: use the "MutableTool.bat" file in the Assets\MutableTool folder
OSX: use the "MutableTool.sh" file in the Assets\MutableTool folder from a terminal window.

Mobile version
-------------
Due to some limitations in the current versions of Unity (no support for native rendering plugins in mobile devices) the performance on mobiles devices is slower when building textures. The same restrictions is also preventing the use of compressed textures in mobile devices.

Contact
-------
Register at our Support Site http://support.anticto.com to get the latest version and documentation of our products. You can also submit bugs or feature requests to our Mutable Tool or the Unity Integration.
You can also contact us directly at contact@anticto.com or bugs@anticto.com. We welcome any feedback and we try to solve any possible bugs immediately.

Changelog
---------

Version 0.7:
- Added first release of the iOS support.
- Added more example model transforms to show how to generate models for iOS to use the builtin Unity shaders.
- Added iOS compiled models for the character (with and without normalmaps) and the helmet.
- Added *-Mobile versions of the sample scenes to be used in iOS.

Version 0.6:
-Removed warnings from the import of resources inside the MutableTool folder (splash images in bmp, and fbx with references to invalid textures).
-Renamed city level citizen creation script to a more generic MutableCreateRandomObjects
-Added multithreaded operation for background construction of models.
-Added initial implementation of data streaming when building customized objects.
-Added Linux plugin.

Version 0.5:
-This version adds support for Mac in the integration plugin.
-The standalone creation tool also supports Mac (64-bit only).
-The script side has been redesigned to support building objects in an asynchronous way in the future releases.
-Updated the stand-alone tool with usability improvements and several critical bug-fixes.

Version 0.4:
- Initial release.

