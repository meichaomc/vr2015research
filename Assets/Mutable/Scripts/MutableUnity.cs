/*
 * ------------------------------------------------------------------------------------------------
 * (c) Anticto Estudi Binari S.L. This file is subject to the terms and conditions defined in
 * the file 'license.txt', which is part of this source code package.
 * ------------------------------------------------------------------------------------------------
 */

using System;
using System.Collections;
using System.Runtime.InteropServices;


using MODEL_HANDLE = System.Int32;
using PARAMETERS_HANDLE = System.Int32;
using INSTANCE_HANDLE = System.Int32;
using MESH_HANDLE = System.Int32;
using IMAGE_HANDLE = System.Int32;
using BUFFERSET_HANDLE = System.Int32;


public class Mutable
{
#if UNITY_IPHONE
	const string _dllLocation = "__Internal";
#else
	const string _dllLocation = "MutableUnity";
#endif

	public enum IMAGE_FORMAT
	{
	  IF_NONE,
	  IF_RGB_UBYTE,
	  IF_RGBA_UBYTE,
	  IF_L_UBYTE,
	  IF_PVRTC2,
	  IF_PVRTC4,
	  IF_ETC1,
	  IF_ETC2,
	  IF_L_UBYTE_RLE,
	  IF_RGB_UBYTE_RLE,
	  IF_RGBA_UBYTE_RLE,
	  IF_L_UBIT_RLE,
	  IF_BC1,
	  IF_BC2,
	  IF_BC3,
	  IF_BC4,
	  IF_BC5,
	  IF_BC6,
	  IF_BC7,
	  IF_BGRA_UBYTE,
	  IF_COUNT
	}

	public enum MESH_BUFFER_FORMAT
	{
	  MBF_NONE,
	  MBF_FLOAT16,
	  MBF_FLOAT32,
	  MBF_UINT8,
	  MBF_UINT16,
	  MBF_UINT32,
	  MBF_INT8,
	  MBF_INT16,
	  MBF_INT32,
	  MBF_NUINT8,
	  MBF_NUINT16,
	  MBF_NUINT32,
	  MBF_NINT8,
	  MBF_NINT16,
	  MBF_NINT32,
	  MBF_PACKEDDIR8,
	  MBF_COUNT
	}

	public enum MESH_BUFFER_SEMANTIC
	{
	  MBS_NONE,
	  MBS_VERTEXINDEX,
	  MBS_POSITION,
	  MBS_NORMAL,
	  MBS_TANGENT,
	  MBS_BINORMAL,
	  MBS_TEXCOORDS,
	  MBS_COLOUR,
	  MBS_BONEWEIGHTS,
	  MBS_BONEINDICES,
	  MBS_LAYOUTBLOCK,
	  MBS_CHART,
	  MBS_OTHER,
	  MBS_TANGENTSIGN,
	  MBS_COUNT
	}

	public enum PARAMETER_TYPE
	{
		T_NONE,
		T_BOOL,
		T_INT,
		T_FLOAT,
		T_COLOUR,
		T_PROJECTOR,
		T_COUNT

	}

	public enum PROFILE_METRIC
	{
		PM_NONE,

		PM_MODEL_COUNT,
		PM_MODEL_BYTES,

		PM_INSTANCE_COUNT,

		PM_CACHE_BYTES,
		PM_STREAMED_BYTES,

		PM_COUNT
	}

	//---------------------------------------------------------------------------------------------
	//---------------------------------------------------------------------------------------------
	[DllImport (_dllLocation)]
	public static extern int mutable_get_error();

	[DllImport (_dllLocation)]
	public static extern IntPtr mutable_get_error_message( int code );


	//---------------------------------------------------------------------------------------------
	[DllImport (_dllLocation )]
	public static extern void mutable_initialise();

	[DllImport (_dllLocation)]
	public static extern bool mutable_is_initialised();

	[DllImport (_dllLocation)]
	public static extern void mutable_finalise();

	[DllImport (_dllLocation)]
	public static extern int mutable_get_profile_metric( PROFILE_METRIC metric );


	//---------------------------------------------------------------------------------------------
	[DllImport (_dllLocation)]
	public static extern MODEL_HANDLE mutable_load_model( byte[] buffer, int size, string streamPrefix, string streamSuffix );

	[DllImport (_dllLocation)]
	public static extern void mutable_free_model( MODEL_HANDLE model );

	[DllImport (_dllLocation)]
	public static extern PARAMETERS_HANDLE mutable_new_parameters( MODEL_HANDLE model );

	[DllImport (_dllLocation)]
	public static extern INSTANCE_HANDLE mutable_new_instance( MODEL_HANDLE model );

	[DllImport (_dllLocation)]
	public static extern int mutable_model_get_state_count( MODEL_HANDLE model );

	[DllImport (_dllLocation)]
	public static extern IntPtr mutable_model_get_state_name( MODEL_HANDLE model, int state );

	[DllImport (_dllLocation)]
	public static extern int mutable_model_get_state_parameter_count( MODEL_HANDLE model, int state );

	[DllImport (_dllLocation)]
	public static extern int mutable_model_get_state_parameter_index( MODEL_HANDLE model, int state, int parameter );


	//---------------------------------------------------------------------------------------------
	[DllImport (_dllLocation)]
	public static extern void mutable_free_parameters( PARAMETERS_HANDLE parameters );

	[DllImport (_dllLocation)]
	public static extern int mutable_parameters_get_count( PARAMETERS_HANDLE handle );

	[DllImport (_dllLocation)]
	public static extern IntPtr mutable_parameters_get_name( PARAMETERS_HANDLE handle, int param );

	[DllImport (_dllLocation)]
	public static extern PARAMETER_TYPE mutable_parameters_get_type( PARAMETERS_HANDLE handle, int param );

	[DllImport (_dllLocation)]
	public static extern bool mutable_parameters_get_bool_value( PARAMETERS_HANDLE handle, int param );

	[DllImport (_dllLocation)]
	public static extern void mutable_parameters_set_bool_value( PARAMETERS_HANDLE handle, int param, bool value );

	[DllImport (_dllLocation)]
	public static extern int mutable_parameters_get_int_value( PARAMETERS_HANDLE handle, int param );

	[DllImport (_dllLocation)]
	public static extern void mutable_parameters_set_int_value( PARAMETERS_HANDLE handle, int param, int value );

	[DllImport (_dllLocation)]
	public static extern int mutable_parameters_get_int_option_count( PARAMETERS_HANDLE handle, int param );

	[DllImport (_dllLocation)]
	public static extern IntPtr mutable_parameters_get_int_option_name( PARAMETERS_HANDLE handle, int param, int option );

	[DllImport (_dllLocation)]
	public static extern int mutable_parameters_get_int_option_value( PARAMETERS_HANDLE handle, int param, int option );

	[DllImport (_dllLocation)]
	public static extern float mutable_parameters_get_float_value( PARAMETERS_HANDLE handle, int param );

	[DllImport (_dllLocation)]
	public static extern void mutable_parameters_set_float_value( PARAMETERS_HANDLE handle, int param, float value );

	[DllImport (_dllLocation)]
	public static extern void mutable_parameters_get_colour_value( PARAMETERS_HANDLE handle, int param,
													  ref float r, ref float g, ref float b );

	[DllImport (_dllLocation)]
	public static extern void mutable_parameters_set_colour_value( PARAMETERS_HANDLE handle, int param,
													  float r, float g, float b );


	//---------------------------------------------------------------------------------------------
	[DllImport (_dllLocation)]
	public static extern void mutable_free_instance( INSTANCE_HANDLE instance );

	[DllImport (_dllLocation)]
	public static extern void mutable_begin_instance_update( INSTANCE_HANDLE instance, PARAMETERS_HANDLE parameters, int state );

	[DllImport (_dllLocation)]
	public static extern void mutable_end_instance_update( INSTANCE_HANDLE instance );

	[DllImport (_dllLocation)]
	public static extern int mutable_get_instance_lod_count( INSTANCE_HANDLE instance );

	[DllImport (_dllLocation)]
	public static extern int mutable_get_instance_component_count( INSTANCE_HANDLE instance, int lod );

	[DllImport (_dllLocation)]
	public static extern IntPtr mutable_get_instance_component_name( INSTANCE_HANDLE instance, int lod, int comp );

	[DllImport (_dllLocation)]
	public static extern int mutable_get_instance_surface_count( INSTANCE_HANDLE instance, int lod, int comp );

	[DllImport (_dllLocation)]
	public static extern IntPtr mutable_get_instance_surface_name( INSTANCE_HANDLE instance, int lod, int comp, int surf );

	[DllImport (_dllLocation)]
	public static extern int mutable_get_instance_mesh_count( INSTANCE_HANDLE instance, int lod, int comp );

	[DllImport (_dllLocation)]
	public static extern IntPtr mutable_get_instance_mesh_name( INSTANCE_HANDLE instance, int lod, int comp, int mesh );

	[DllImport (_dllLocation)]
	public static extern MESH_HANDLE mutable_get_instance_mesh( INSTANCE_HANDLE instance, int lod, int comp, int mesh );

	[DllImport (_dllLocation)]
	public static extern int mutable_get_instance_image_count( INSTANCE_HANDLE instance, int lod, int comp, int surf );

	[DllImport (_dllLocation)]
	public static extern IntPtr mutable_get_instance_image_name( INSTANCE_HANDLE instance, int lod, int comp, int surf, int image );

	[DllImport (_dllLocation)]
	public static extern IMAGE_HANDLE mutable_get_instance_image( INSTANCE_HANDLE instance, int lod, int comp, int surf, int image );


	//---------------------------------------------------------------------------------------------
	[DllImport (_dllLocation)]
	public static extern void mutable_free_mesh( MESH_HANDLE mesh );

	[DllImport (_dllLocation)]
	public static extern uint mutable_get_mesh_id( MESH_HANDLE mesh );

	[DllImport (_dllLocation)]
	public static extern int mutable_get_mesh_updated( MESH_HANDLE mesh );

	[DllImport (_dllLocation)]
	public static extern BUFFERSET_HANDLE mutable_get_mesh_vertexbuffers( MESH_HANDLE mesh );

	[DllImport (_dllLocation)]
	public static extern BUFFERSET_HANDLE mutable_get_mesh_indexbuffers( MESH_HANDLE mesh );

	[DllImport (_dllLocation)]
	public static extern int mutable_get_mesh_surface_count( MESH_HANDLE mesh );

	[DllImport (_dllLocation)]
	public static extern int mutable_get_mesh_surface_first_vertex( MESH_HANDLE mesh, int surf );

	[DllImport (_dllLocation)]
	public static extern int mutable_get_mesh_surface_vertex_count( MESH_HANDLE mesh, int surf );

	[DllImport (_dllLocation)]
	public static extern int mutable_get_mesh_surface_first_index( MESH_HANDLE mesh, int surf );

	[DllImport (_dllLocation)]
	public static extern int mutable_get_mesh_surface_index_count( MESH_HANDLE mesh, int surf );

	[DllImport (_dllLocation)]
	public static extern int mutable_get_mesh_bone_count( MESH_HANDLE mesh );

	[DllImport (_dllLocation)]
	public static extern IntPtr mutable_get_mesh_bone_name( MESH_HANDLE mesh, int boneIndex );

	[DllImport (_dllLocation)]
	public static extern void mutable_get_mesh_bone_matrix( MESH_HANDLE mesh, int boneIndex, float[] mat4x4 );


	//---------------------------------------------------------------------------------------------
	[DllImport (_dllLocation)]
	public static extern void mutable_free_bufferset( BUFFERSET_HANDLE mesh );

	[DllImport (_dllLocation)]
	public static extern int mutable_get_bufferset_element_count( BUFFERSET_HANDLE bufferset );

	[DllImport (_dllLocation)]
	public static extern int mutable_get_bufferset_buffer_count( BUFFERSET_HANDLE bufferset );

	[DllImport (_dllLocation)]
	public static extern int mutable_get_bufferset_channel_count( BUFFERSET_HANDLE bufferset, int buffer );

	[DllImport (_dllLocation)]
	public static extern int mutable_get_bufferset_element_size( BUFFERSET_HANDLE bufferset, int buffer );

	[DllImport (_dllLocation)]
	public static extern MESH_BUFFER_SEMANTIC mutable_get_bufferset_channel_semantic( BUFFERSET_HANDLE bufferset, int buffer, int channel );

	[DllImport (_dllLocation)]
	public static extern int mutable_get_bufferset_channel_semantic_index( BUFFERSET_HANDLE bufferset, int buffer, int channel );

	[DllImport (_dllLocation)]
	public static extern MESH_BUFFER_FORMAT mutable_get_bufferset_channel_format( BUFFERSET_HANDLE bufferset, int buffer, int channel );

	[DllImport (_dllLocation)]
	public static extern int mutable_get_bufferset_channel_components( BUFFERSET_HANDLE bufferset, int buffer, int channel );

	[DllImport (_dllLocation)]
	public static extern int mutable_get_bufferset_channel_offset( BUFFERSET_HANDLE bufferset, int buffer, int channel );

	[DllImport (_dllLocation)]
	public static extern IntPtr mutable_get_bufferset_data( BUFFERSET_HANDLE bufferset, int buffer );

	[DllImport (_dllLocation)]
	public static extern void mutable_get_bufferset_channel_data_float( BUFFERSET_HANDLE bufferset, int buffer, int channel, float[] data );

	[DllImport (_dllLocation)]
	public static extern void mutable_get_bufferset_channel_data_int( BUFFERSET_HANDLE bufferset, int buffer, int channel, int[] data );

	//---------------------------------------------------------------------------------------------
	[DllImport (_dllLocation)]
	public static extern void mutable_free_image( IMAGE_HANDLE image );

	[DllImport (_dllLocation)]
	public static extern uint mutable_get_image_id( IMAGE_HANDLE image );

	[DllImport (_dllLocation)]
	public static extern int mutable_get_image_updated( IMAGE_HANDLE image );

	[DllImport (_dllLocation)]
	public static extern IMAGE_FORMAT mutable_get_image_format( IMAGE_HANDLE image );

	[DllImport (_dllLocation)]
	public static extern int mutable_get_image_lod_count( IMAGE_HANDLE image );

	[DllImport (_dllLocation)]
	public static extern int mutable_get_image_size_x( IMAGE_HANDLE image );

	[DllImport (_dllLocation)]
	public static extern int mutable_get_image_size_y( IMAGE_HANDLE image );

	[DllImport (_dllLocation)]
	public static extern int mutable_get_image_datasize( IMAGE_HANDLE image );

	[DllImport (_dllLocation)]
	public static extern IntPtr mutable_get_image_data( IMAGE_HANDLE image );

	[DllImport (_dllLocation)]
	public static extern void mutable_get_image_data_uint8( IMAGE_HANDLE image, byte[] data, int mip );

	[DllImport (_dllLocation)]
	public static extern void mutable_copy_image( IMAGE_HANDLE image, IntPtr nativeTexturePtr );

	[DllImport (_dllLocation)]
	public static extern int mutable_has_native_texture_copy_support();
}


