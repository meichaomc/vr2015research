/*
 * ------------------------------------------------------------------------------------------------
 * (c) Anticto Estudi Binari S.L. This file is subject to the terms and conditions defined in 
 * the file 'license.txt', which is part of this package.
 * ------------------------------------------------------------------------------------------------
 */

using UnityEngine;

using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Collections.Generic;

// Used for debugging purposes
public class Trace
{
	public static void Log( string msg )
	{
		//Debug.Log(msg);
	}
}


// Encapsulation of a call to the Mutable thread (when it exists) from the main thread.
// Only the code here should access the Mutable Runtime Interface.
public class MutableCommand
{
	// The id of the thread that can access the mutable api
	public static int m_threadId = -1;
	
	// This is true if a command already in the pending queue has been cancelled by a later command.
	public bool cancelled = false;
	
	public enum Type 
	{
		INIT_SYSTEM,
		INIT_MODEL,
		BEGIN_UPDATE_INSTANCE,
		UPDATE_INSTANCE,
		
		// Issued from the unity thread when the results of the update of an instance have been processed
		COMPLETED_UPDATE_INSTANCE,
		
		// Issued from the unity thread when we want to cancel the updates of a specifc instance.
		// If the instance is null, all updates are cancelled
		CANCEL_UPDATES,
		
		END_UPDATE_INSTANCE,
		RELEASE_IMAGE,
		RELEASE_MODEL,
		RELEASE_SYSTEM
	}
	
	public Type type;
	public MutableModel model;
    public byte[] modelBuffer;				
	
	public MutableInstance instance;		
	public MutableManager manager;	
	
	// For image operations
	public int imageHandle;
	
	
	private void CheckThread()
	{
		System.Diagnostics.Debug.Assert( m_threadId==System.Threading.Thread.CurrentThread.ManagedThreadId );		
	}
	
	
	public void Execute()
	{
		CheckThread();
		
		if (cancelled)
		{
			return;
		}
		
		switch( type )
		{
		case Type.INIT_SYSTEM:
		{
			if ( !Mutable.mutable_is_initialised() )
			{
		    	Mutable.mutable_initialise();
	    		MutableCommand.check_error();
			}
			break;
		}
			
		case Type.RELEASE_SYSTEM:
		{			
			foreach( int instanceHandle in manager.m_pendingImagesToFree )
			{
				Mutable.mutable_free_image( instanceHandle );
		        MutableCommand.check_error();
			}
			manager.m_pendingImagesToFree.Clear();
			
			foreach( int instanceHandle in manager.m_instances )
			{
				Mutable.mutable_free_instance( instanceHandle );
		        MutableCommand.check_error();
			}
			manager.m_instances.Clear();
			
			foreach( int parametersHandle in manager.m_parameters )
			{
				Mutable.mutable_free_parameters( parametersHandle );
		        MutableCommand.check_error();
			}
			manager.m_parameters.Clear();
			
	        Mutable.mutable_finalise();
	        MutableCommand.check_error();
			break;
		}						
			
		case Type.INIT_MODEL:
			// This forces the read of the static data for the model.
			// It will currently happen for all models at the beginning of the scene, forcing its load
			// which may be inconvenient. 
			// \TODO: Look for a way to do this before and store the data. Ideally it should happen 
			// at scene compilation time.
			CreateModelHandle();
			ReleaseModelHandle();		
			break;
			
		case Type.BEGIN_UPDATE_INSTANCE:		
			Trace.Log( "MutableCommand.ExecuteCommand case BEGIN_UPDATE_INSTANCE" );
			model = instance.m_model;
			instance.m_modelHandle = CreateModelHandle();
			if (instance.m_modelHandle>=0)
			{
				instance.m_importScaleFactor = instance.m_model.ImportScaleFactor;
			
		        instance.m_instance = NewInstance( instance.m_modelHandle );
		
				if (instance.m_instance>=0)
				{
					instance.m_parameters = NewParameters( instance.m_modelHandle );
				}
			}
			break;
			
		case Type.UPDATE_INSTANCE:		
		{
			Trace.Log( "MutableCommand.ExecuteCommand case UPDATE_INSTANCE" );
			SetParameterValues( instance.m_parameters, instance.parameters );					
			Mutable.mutable_begin_instance_update( instance.m_instance, instance.m_parameters, instance.state );
	        check_error();
			
			int lod = 0;
			
			// Collect update results from mutable
			int componentCount = Mutable.mutable_get_instance_component_count( instance.m_instance, lod );
	        check_error();
			
			instance.components = new MutableInstance.COMPONENT[componentCount];
			
			for ( int c=0; c<componentCount; ++c )
			{
				instance.components[c].name = Marshal.PtrToStringAnsi( Mutable.mutable_get_instance_component_name( instance.m_instance, lod, c ) );
				MutableCommand.check_error();
	
				int meshCount = Mutable.mutable_get_instance_mesh_count( instance.m_instance, lod, c );
		       	MutableCommand.check_error();
				instance.components[c].meshes = new MutableInstance.MESH[meshCount];
				
				for ( int m=0; m<meshCount; ++m )
				{
					int meshHandle = Mutable.mutable_get_instance_mesh( instance.m_instance, lod, c, m );
			        MutableCommand.check_error();
					
					uint meshId = Mutable.mutable_get_mesh_id(meshHandle);
					instance.components[c].meshes[m].meshId = meshId;
					instance.components[c].meshes[m].reused = Mutable.mutable_get_mesh_updated(meshHandle)==0;
					
					if ( !instance.components[c].meshes[m].reused 
						||
						!instance.m_idToMesh.ContainsKey(meshId) )
					{				
						ConvertMesh_Mutable( meshHandle, instance.m_importScaleFactor, ref instance.components[c].meshes[m] );					
					}
	
					instance.components[c].meshes[m].name = Marshal.PtrToStringAnsi( Mutable.mutable_get_instance_mesh_name( instance.m_instance, lod, c, m ) );
		       		MutableCommand.check_error();
	
					Mutable.mutable_free_mesh( meshHandle );
			        MutableCommand.check_error();
				}
	
				int surfaceCount = Mutable.mutable_get_instance_surface_count( instance.m_instance, lod, c );
		       	MutableCommand.check_error();
				instance.components[c].materials = new MutableInstance.MATERIAL[surfaceCount];
				
				for ( int s=0; s<surfaceCount; ++s )
				{
					int imageCount = Mutable.mutable_get_instance_image_count( instance.m_instance, lod, c, s );
			       	MutableCommand.check_error();
					instance.components[c].materials[s].textures = new MutableInstance.TEXTURE[imageCount];
					
					for ( int i=0; i<imageCount; ++i )
					{	
						// If native image support is found, the mutable image will not be freed until it has been 
						// uploaded so we need the native texture pointer which has to be created in the unity thread.
						int imageHandle = Mutable.mutable_get_instance_image( instance.m_instance, lod, c, s, i );
				        MutableCommand.check_error();
	
						uint imageId = Mutable.mutable_get_image_id( imageHandle );
						instance.components[c].materials[s].textures[i].imageId = imageId;
						instance.components[c].materials[s].textures[i].reused = Mutable.mutable_get_image_updated( imageHandle )==0;
						
						if ( !instance.components[c].materials[s].textures[i].reused )
						{				
							ConvertImage_Mutable( imageHandle, ref instance.components[c].materials[s].textures[i] );
							
							// If we have the pixels, we are not converting natively, we have to release the image here
							// since it won't be done in the image copy command.
							if ( instance.components[c].materials[s].textures[i].pixels!=null)
							{
								Mutable.mutable_free_image( imageHandle );
							}
							else
							{
								manager.m_pendingImagesToFree.Add( imageHandle );
							}
						}
						else
						{
							Mutable.mutable_free_image( imageHandle );
						}	
	
						instance.components[c].materials[s].textures[i].name = Marshal.PtrToStringAnsi( Mutable.mutable_get_instance_image_name( instance.m_instance, lod, c, s, i ) );
			       		MutableCommand.check_error();
					}			
				}
			}
						
			Mutable.mutable_end_instance_update( instance.m_instance );
	        MutableCommand.check_error();
			
			break;
		}
			
			
		case Type.RELEASE_IMAGE:		
		{
			Mutable.mutable_free_image( imageHandle );
			manager.m_pendingImagesToFree.Remove( imageHandle );
			break;
		}
			
		case Type.END_UPDATE_INSTANCE:	
			Trace.Log( "MutableCommand.ExecuteCommand case END_UPDATE_INSTANCE" );
			model = instance.m_model;
			ReleaseInstance( instance.m_instance );
			instance.m_instance = -1;
	
			ReleaseParameters( instance.m_parameters );
			instance.m_parameters = -1;
			
			ReleaseModelHandle();
			instance.m_modelHandle = -1;					
			break;
			
		case Type.RELEASE_MODEL:
			Mutable.mutable_free_model( model.m_handle );
	        check_error();			
			model.m_handle = -1;			
			break;
		}
	}
	
		
	// Check that there hasn't been any API or Mutable internal error.
    public static bool check_error()
    {
		bool ok = true;
		
        int error = Mutable.mutable_get_error();
        if (error!=0)
        {
            Debug.LogWarning( "Mutable error : "
				+ Marshal.PtrToStringAnsi( Mutable.mutable_get_error_message( error ) ) );
			//Debug.DebugBreak();
			
			ok=false;
        }
		
		return ok;
    }
	
	
	
	// This methods sets the data in a script-side paramter values to the
	// plugin-side parameters data.
	private void SetParameterValues( int parametersHandle, MutableParameters parameters )
	{
		int mutableParamCount = Mutable.mutable_parameters_get_count( parametersHandle );
		check_error();
		
		for ( int paramIndex=0; paramIndex<mutableParamCount; ++paramIndex )
		{
			string paramName = Marshal.PtrToStringAnsi( Mutable.mutable_parameters_get_name( parametersHandle, paramIndex ) );
			check_error();
			
			Mutable.PARAMETER_TYPE paramType = Mutable.mutable_parameters_get_type( parametersHandle, paramIndex );
			check_error();
			
			switch (paramType)
			{
			case Mutable.PARAMETER_TYPE.T_BOOL:	
			{
				bool v;
				if ( parameters.GetBoolValue(paramName,out v) )
				{
					Mutable.mutable_parameters_set_bool_value( parametersHandle, paramIndex, v );
					check_error();
				}
				break;
			}
				
			case Mutable.PARAMETER_TYPE.T_FLOAT:	
			{
				float v;
				if ( parameters.GetFloatValue(paramName,out v) )
				{
					Mutable.mutable_parameters_set_float_value( parametersHandle, paramIndex, v );
					check_error();
				}
				break;
			}
				
			case Mutable.PARAMETER_TYPE.T_COLOUR:	
			{
				Vector3 v;
				if ( parameters.GetColourValue(paramName,out v) )
				{
					Mutable.mutable_parameters_set_colour_value( parametersHandle, paramIndex, v.x, v.y, v.z );
					check_error();
				}
				break;
			}
				
			case Mutable.PARAMETER_TYPE.T_INT:	
			{
				int v;
				if ( parameters.GetIntValue(paramName,out v) )
				{
					Mutable.mutable_parameters_set_int_value( parametersHandle, paramIndex, v );
					check_error();
				}
				break;
			}
				
			default: 
				Debug.Log( "Unsupported parameter type : " + paramType );
				break;	
			}
		}
	}	
	
	
	//!
	private int NewInstance( int modelHandle )
	{
        int instance = Mutable.mutable_new_instance( modelHandle );
        check_error();
		
		manager.m_instances.Add( instance );
		
		return instance;
	}
	
	
	//!
	private void ReleaseInstance( int instanceHandle )
	{
		if (Mutable.mutable_is_initialised())
		{
			Mutable.mutable_free_instance( instanceHandle );
	        check_error();
		}
		
		manager.m_instances.Remove( instanceHandle );
	}
	
	
	//!
	private int NewParameters( int modelHandle )
	{
        int parameters = Mutable.mutable_new_parameters( modelHandle );
        check_error();
		
		manager.m_parameters.Add( parameters );
		
		return parameters;
	}
	
	
	//!
	private void ReleaseParameters( int parametersHandle )
	{
		if (Mutable.mutable_is_initialised())
		{
			Mutable.mutable_free_parameters( parametersHandle );
	        check_error();
		}
		
		manager.m_parameters.Remove( parametersHandle );
	}
	
	
	//
	private int CreateModelHandle()
	{
		if (model.m_handle<0)
		{
			if ( modelBuffer!=null )
			{
				string streamPrefix = model.m_streamingPrefix;
				
		        model.m_handle = Mutable.mutable_load_model( modelBuffer, modelBuffer.Length, 
					streamPrefix,
					model.ModelDataStreamingSuffix );
				bool ok = MutableCommand.check_error();
				if (!ok)
				{
		            Debug.LogError("Failed to load mutable model from binary data in Mutable Model component.");
					Debug.DebugBreak();
					
					model.m_handle = -1;
				}
				else				
				{
					// Read the static data to be cached in script side. This includes states, parameter
					// descriptions, and default parameter values.
					UpdateModelFromPluginModel();
				}
			}
			else
			{
	            Debug.LogError("Failed to load mutable model: no TextAsset assigned.");
				Debug.DebugBreak();
			}
		}
		
		if (model.m_handle>=0)
		{
			++model.m_references;
		}
		
		return model.m_handle;
	}
	
	
	// Tell the manager that this model is no longer in use. It won't be unloaded from memory.
	private void ReleaseModelHandle()
	{
		if (model.m_references<=0)
		{
			Debug.LogError(  "Mismatched GetModel/ReleaseModel calls." );
			Debug.DebugBreak();
		}	
		else
		{
			model.m_references--;
		}
	}
	
	
	//
	private void UpdateModelFromPluginModel ()
	{
		int modelHandle = model.m_handle;
			
		if (modelHandle>=0)
		{
			int parametersHandle = NewParameters( modelHandle );
			
			// Get the state information
			int stateCount = Mutable.mutable_model_get_state_count( modelHandle );
			check_error();
			
			model.states = new MutableModel.STATE[stateCount];
			for ( int s=0; s<stateCount; ++s )
			{
				model.states[s].name = Marshal.PtrToStringAnsi( Mutable.mutable_model_get_state_name( modelHandle, s ) );
				check_error();
	
				int paramCount = Mutable.mutable_model_get_state_parameter_count( modelHandle, s );
				check_error();
				
				model.states[s].runtimeParameters = new int[paramCount];
				for ( int p=0; p<paramCount; ++p )
				{
					model.states[s].runtimeParameters[p] = Mutable.mutable_model_get_state_parameter_index( modelHandle, s, p );
					check_error();
				}
			}
			
			// Convert the parameter description
			int parameterCount = Mutable.mutable_parameters_get_count( parametersHandle );
			check_error();
	
			model.parameterDescriptions = new MutableModel.PARAMETER_DESC[parameterCount];
			int boolCount=0, floatCount=0, colourCount=0, intCount=0;
			
			for ( int s=0; s<parameterCount; ++s )
			{
				model.parameterDescriptions[s].name = Marshal.PtrToStringAnsi( Mutable.mutable_parameters_get_name( parametersHandle, s ) );
				model.parameterDescriptions[s].type = Mutable.mutable_parameters_get_type( parametersHandle, s );
				switch (model.parameterDescriptions[s].type)
				{
				case Mutable.PARAMETER_TYPE.T_FLOAT: 
					floatCount++;
					break;
					
				case Mutable.PARAMETER_TYPE.T_BOOL: 
					boolCount++;
					break;
					
				case Mutable.PARAMETER_TYPE.T_COLOUR: 
					colourCount++;
					break;
					
				case Mutable.PARAMETER_TYPE.T_INT: 
					intCount++;
					
					int optionCount = Mutable.mutable_parameters_get_int_option_count( parametersHandle, s );
					check_error();
					
					model.parameterDescriptions[s].intOptions = new string[optionCount];
					model.parameterDescriptions[s].intValues = new int[optionCount];
					for ( int o=0; o<optionCount; ++o )
					{
						model.parameterDescriptions[s].intOptions[o] = Marshal.PtrToStringAnsi( Mutable.mutable_parameters_get_int_option_name( parametersHandle, s, o ) );
						model.parameterDescriptions[s].intValues[o] = Mutable.mutable_parameters_get_int_option_value( parametersHandle, s, o );
					}
					break;
					
				default: Debug.Log( "Unsupported parameter type : " + model.parameterDescriptions[s].type ); break;
				}
			}		
			
			// Convert the parameter values
			model.defaultParameterValues = new MutableParameters();
			model.defaultParameterValues.bools = new MutableParameters.BOOL_PARAMETER[boolCount];
			model.defaultParameterValues.floats = new MutableParameters.FLOAT_PARAMETER[floatCount];
			model.defaultParameterValues.colours = new MutableParameters.COLOUR_PARAMETER[colourCount];
			model.defaultParameterValues.ints = new MutableParameters.INT_PARAMETER[intCount];
			int boolCurrent=0, floatCurrent=0, colourCurrent=0, intCurrent=0;
			for ( int s=0; s<parameterCount; ++s )
			{
				switch (model.parameterDescriptions[s].type)
				{
				case Mutable.PARAMETER_TYPE.T_FLOAT: 
					model.defaultParameterValues.floats[floatCurrent].name = model.parameterDescriptions[s].name;
					model.defaultParameterValues.floats[floatCurrent].floatValue = Mutable.mutable_parameters_get_float_value( parametersHandle, s ); 
					check_error();
					++floatCurrent;
					break;
					
				case Mutable.PARAMETER_TYPE.T_BOOL: 
					model.defaultParameterValues.bools[boolCurrent].name = model.parameterDescriptions[s].name;
					model.defaultParameterValues.bools[boolCurrent].boolValue = Mutable.mutable_parameters_get_bool_value( parametersHandle, s ); 
					check_error();
					++boolCurrent;
					break;
					
				case Mutable.PARAMETER_TYPE.T_COLOUR: 
					float r=0,g=0,b=0;

					Mutable.mutable_parameters_get_colour_value( parametersHandle, s, ref r, ref g, ref b ); 

					model.defaultParameterValues.colours[colourCurrent].name = model.parameterDescriptions[s].name;
					model.defaultParameterValues.colours[colourCurrent].colourValue = new Vector3(r,g,b);
					check_error();
					++colourCurrent;
					break;
					
				case Mutable.PARAMETER_TYPE.T_INT: 
					model.defaultParameterValues.ints[intCurrent].name = model.parameterDescriptions[s].name;
					model.defaultParameterValues.ints[intCurrent].intValue = Mutable.mutable_parameters_get_int_value( parametersHandle, s ); 
					check_error();
					++intCurrent;
					break;
					
				default: Debug.Log( "Unsupported parameter type : " + model.parameterDescriptions[s].type ); break;
				}
			}		
			
			// Free the temp resources
			ReleaseParameters( parametersHandle );			
		}
	}
	
	
	//!
	private static void ConvertMesh_Mutable( 
		int meshHandle, 
		float importScaleFactor,
		ref MutableInstance.MESH meshData )
	{				
		// Bones
		{
			int boneCount = Mutable.mutable_get_mesh_bone_count( meshHandle );
	        MutableCommand.check_error();
			
			meshData.bones = new MutableInstance.BONE[boneCount];			
			//Matrix4x4[] bposes = new Matrix4x4[boneCount];
			
			for (int b=0; b<boneCount; ++b)
			{
				meshData.bones[b].name = Marshal.PtrToStringAnsi( Mutable.mutable_get_mesh_bone_name( meshHandle, b ) );
	        	MutableCommand.check_error();
							
/*							
				float[] mat4x4 = new float[16];
				Mutable.mutable_get_mesh_bone_matrix( meshHandle, b, mat4x4 );
	        	MutableManager.check_error();
								
				bposes[b].m00 = mat4x4[0];
				bposes[b].m01 = mat4x4[1];
				bposes[b].m02 = mat4x4[2];
				bposes[b].m03 = mat4x4[3];
				
				bposes[b].m10 = mat4x4[4];
				bposes[b].m11 = mat4x4[5];
				bposes[b].m12 = mat4x4[6];
				bposes[b].m13 = mat4x4[7];
				
				bposes[b].m20 = mat4x4[8];
				bposes[b].m21 = mat4x4[9];
				bposes[b].m22 = mat4x4[10];
				bposes[b].m23 = mat4x4[11];
				
				bposes[b].m30 = mat4x4[12];
				bposes[b].m31 = mat4x4[13];
				bposes[b].m32 = mat4x4[14];
				bposes[b].m33 = mat4x4[15];	
				
				meshData.bones[b].bindPose = bposes[b];
				*/
			}
			
			//mesh.bindposes = bposes;
		}

		// Vertices
		{
			int vertexHandle = Mutable.mutable_get_mesh_vertexbuffers( meshHandle );
	        MutableCommand.check_error();
			
			int vertexCount = Mutable.mutable_get_bufferset_element_count( vertexHandle );
	        MutableCommand.check_error();				
			
			int bufferCount = Mutable.mutable_get_bufferset_buffer_count( vertexHandle );
	        MutableCommand.check_error();
		
			BoneWeight[] boneWeights = new BoneWeight[vertexCount];
			Vector3[] vertices = new Vector3[vertexCount];
			Vector3[] normals = new Vector3[vertexCount];
			Vector4[] tangents = null;
			Vector2[] uvs0 = null;
			Vector2[] uvs1 = null;
			
			for ( int buf=0; buf<bufferCount; ++buf )
			{
				int channelCount = Mutable.mutable_get_bufferset_channel_count( vertexHandle, buf );
		        MutableCommand.check_error();
					
				for ( int channel=0; channel<channelCount; ++channel )
				{
					Mutable.MESH_BUFFER_SEMANTIC semantic = Mutable.mutable_get_bufferset_channel_semantic( vertexHandle, buf, channel );
	        		MutableCommand.check_error();

					int semanticIndex = Mutable.mutable_get_bufferset_channel_semantic_index( vertexHandle, buf, channel );	
	        		MutableCommand.check_error();
										
					if ( semantic==Mutable.MESH_BUFFER_SEMANTIC.MBS_POSITION && semanticIndex==0 )
					{
						int compCount = Mutable.mutable_get_bufferset_channel_components( vertexHandle, buf, channel );
	        			MutableCommand.check_error();
						
						float[] sourceFloats = new float[vertexCount*compCount];

						Mutable.mutable_get_bufferset_channel_data_float( vertexHandle, buf, channel, sourceFloats );
	        			MutableCommand.check_error();
						
						for ( int v=0; v<vertexCount; ++v )
						{
							vertices[v].x = sourceFloats[v*compCount+0];
							vertices[v].y = sourceFloats[v*compCount+1];
							vertices[v].z = sourceFloats[v*compCount+2];
							
							vertices[v] *= importScaleFactor;
							vertices[v].x *= -1.0f;
						}						
					}
					
					else if ( semantic==Mutable.MESH_BUFFER_SEMANTIC.MBS_NORMAL && semanticIndex==0 )
					{
						int compCount = Mutable.mutable_get_bufferset_channel_components( vertexHandle, buf, channel );
	        			MutableCommand.check_error();
						
						float[] sourceFloats = new float[vertexCount*compCount];

						Mutable.mutable_get_bufferset_channel_data_float( vertexHandle, buf, channel, sourceFloats );
	        			MutableCommand.check_error();
						
						for ( int v=0; v<vertexCount; ++v )
						{
							normals[v].x = sourceFloats[v*compCount+0];
							normals[v].y = sourceFloats[v*compCount+1];
							normals[v].z = sourceFloats[v*compCount+2];
							normals[v].x *= -1.0f;
						}
					}
					
					else if ( semantic==Mutable.MESH_BUFFER_SEMANTIC.MBS_TANGENT && semanticIndex==0 )
					{
						int compCount = Mutable.mutable_get_bufferset_channel_components( vertexHandle, buf, channel );
	        			MutableCommand.check_error();
						
						float[] sourceFloats = new float[vertexCount*compCount];

						Mutable.mutable_get_bufferset_channel_data_float( vertexHandle, buf, channel, sourceFloats );
	        			MutableCommand.check_error();
						
						if (tangents==null)
						{
							tangents = new Vector4[vertexCount];
						}
						
						for ( int v=0; v<vertexCount; ++v )
						{
							tangents[v].x = sourceFloats[v*compCount+0];
							tangents[v].y = sourceFloats[v*compCount+1];
							tangents[v].z = sourceFloats[v*compCount+2];
							tangents[v].x *= -1.0f;
						}						
					}
					
					else if ( semantic==Mutable.MESH_BUFFER_SEMANTIC.MBS_TANGENTSIGN && semanticIndex==0 )
					{
						int compCount = Mutable.mutable_get_bufferset_channel_components( vertexHandle, buf, channel );
	        			MutableCommand.check_error();
						
						float[] sourceFloats = new float[vertexCount*compCount];

						Mutable.mutable_get_bufferset_channel_data_float( vertexHandle, buf, channel, sourceFloats );
	        			MutableCommand.check_error();
						
						if (tangents==null)
						{
							tangents = new Vector4[vertexCount];
						}
						
						for ( int v=0; v<vertexCount; ++v )
						{
							tangents[v].w = sourceFloats[v*compCount+0];
						}						
					}
					
					else if ( semantic==Mutable.MESH_BUFFER_SEMANTIC.MBS_TEXCOORDS )
					{
						int compCount = Mutable.mutable_get_bufferset_channel_components( vertexHandle, buf, channel );
	        			MutableCommand.check_error();
						
						float[] sourceFloats = new float[vertexCount*compCount];

						Mutable.mutable_get_bufferset_channel_data_float( vertexHandle, buf, channel, sourceFloats );
	        			MutableCommand.check_error();
						
						Vector2[] uvs = new Vector2[vertexCount];
						for ( int v=0; v<vertexCount; ++v )
						{
							uvs[v].x = sourceFloats[v*compCount+0];
							uvs[v].y = sourceFloats[v*compCount+1];
						}
						
						if (semanticIndex==0)
						{
							uvs0 = uvs;
						}
						else if (semanticIndex==1)
						{
							uvs1 = uvs;
						}
					}
					
					else if ( semantic==Mutable.MESH_BUFFER_SEMANTIC.MBS_BONEWEIGHTS && semanticIndex==0 )
					{
						int compCount = Mutable.mutable_get_bufferset_channel_components( vertexHandle, buf, channel );
	        			MutableCommand.check_error();
						
						float[] sourceFloats = new float[vertexCount*compCount];

						Mutable.mutable_get_bufferset_channel_data_float( vertexHandle, buf, channel, sourceFloats );
	        			MutableCommand.check_error();
						
						for ( int v=0; v<vertexCount; ++v )
						{
							float accum = 0.0f;
							if (compCount>=1)
							{
								boneWeights[v].weight0 = sourceFloats[v*compCount+0];
								accum += boneWeights[v].weight0;
							}
									
							if (compCount>=2)
							{
								boneWeights[v].weight1 = sourceFloats[v*compCount+1];
								accum += boneWeights[v].weight1;
							}
									
							if (compCount>=3)
							{
								boneWeights[v].weight2 = sourceFloats[v*compCount+2];
								accum += boneWeights[v].weight2;
							}
									
							if (compCount>=4)
							{
								boneWeights[v].weight3 = sourceFloats[v*compCount+3];
								accum += boneWeights[v].weight3;
							}	
														
							boneWeights[v].weight0 /= accum;
							boneWeights[v].weight1 /= accum;
							boneWeights[v].weight2 /= accum;
							boneWeights[v].weight3 /= accum;
							
						}
					}
					
					else if ( semantic==Mutable.MESH_BUFFER_SEMANTIC.MBS_BONEINDICES && semanticIndex==0 )
					{
						int compCount = Mutable.mutable_get_bufferset_channel_components( vertexHandle, buf, channel );
	        			MutableCommand.check_error();
						
						int[] sourceInts = new int[vertexCount*compCount];

						Mutable.mutable_get_bufferset_channel_data_int( vertexHandle, buf, channel, sourceInts );
	        			MutableCommand.check_error();
						
						for ( int v=0; v<vertexCount; ++v )
						{
							if (compCount>=1)
							{
								boneWeights[v].boneIndex0 = sourceInts[v*compCount+0];
							}
									
							if (compCount>=2)
							{
								boneWeights[v].boneIndex1 = sourceInts[v*compCount+1];
							}
									
							if (compCount>=3)
							{
								boneWeights[v].boneIndex2 = sourceInts[v*compCount+2];
							}
									
							if (compCount>=4)
							{
								boneWeights[v].boneIndex3 = sourceInts[v*compCount+3];
							}							
						}
					}
					
					else
					{
						//Debug.Log( "Mesh has unsupported channel ["+semantic+"] with index "+semanticIndex );
					}
										
				}
				
				// debug
				/*
				foreach( var v in boneWeights )
				{
					Debug.Log( v.boneIndex0+" "+v.boneIndex1+" "+v.boneIndex2+" "+v.boneIndex3+" "+v.weight0+" "+v.weight1+" "+v.weight2+" "+v.weight3);
				}
				*/
			}
			
			meshData.boneWeights = boneWeights;
			meshData.vertices = vertices;
			meshData.normals = normals;
			meshData.tangents = tangents;
			meshData.uvs0 = uvs0;
			meshData.uvs1 = uvs1;
			
			Mutable.mutable_free_bufferset( vertexHandle );
        	MutableCommand.check_error();
		}
		
	
		// Indices
		{
			int indexHandle = Mutable.mutable_get_mesh_indexbuffers( meshHandle );
	        MutableCommand.check_error();
			
			int indexCount = Mutable.mutable_get_bufferset_element_count( indexHandle );
	        MutableCommand.check_error();
							
			int bufferCount = Mutable.mutable_get_bufferset_buffer_count( indexHandle );
	        MutableCommand.check_error();
			for ( int buf=0; buf<bufferCount; ++buf )
			{
				int channelCount = Mutable.mutable_get_bufferset_channel_count( indexHandle, buf );
		        MutableCommand.check_error();
				for ( int channel=0; channel<channelCount; ++channel )
				{
					Mutable.MESH_BUFFER_SEMANTIC semantic = Mutable.mutable_get_bufferset_channel_semantic( indexHandle, buf, channel );
	        		MutableCommand.check_error();
					
					int semanticIndex = Mutable.mutable_get_bufferset_channel_semantic_index( indexHandle, buf, channel );
        			MutableCommand.check_error();						
					
					//int compCount = Mutable.mutable_get_bufferset_channel_components( indexHandle, buf, channel );
        			//MutableManager.check_error();
					
					if ( semantic==Mutable.MESH_BUFFER_SEMANTIC.MBS_VERTEXINDEX && semanticIndex==0 )
					{
						int[] indices = new int[indexCount];
						Mutable.mutable_get_bufferset_channel_data_int( indexHandle, buf, channel, indices );
	        			MutableCommand.check_error();
								
						// For materials
						int surfCount = Mutable.mutable_get_mesh_surface_count( meshHandle );
        				MutableCommand.check_error();
						
						// TODO Another hack to optimise
						for (int i=0; i<indexCount/3; ++i)
						{
							int t = indices[i*3+0];
							indices[i*3+0] = indices[i*3+2];
							indices[i*3+2] = t;
						}
						
						meshData.indices = indices;

						if (surfCount>0)
						{
							meshData.surfaces = new MutableInstance.MESH.SURFACE[surfCount];
							
							for (int s=0;s<surfCount; ++s)
							{
								meshData.surfaces[s].firstIndex = Mutable.mutable_get_mesh_surface_first_index( meshHandle, s );
								MutableCommand.check_error();
								
								meshData.surfaces[s].count = Mutable.mutable_get_mesh_surface_index_count( meshHandle, s );
								MutableCommand.check_error();
							}
						}
					}
				}
			}
			
			Mutable.mutable_free_bufferset( indexHandle );
        	MutableCommand.check_error();
		}
		
	}

	
		
	
	//---------------------------------------------------------------------------------------------
	//!
	//---------------------------------------------------------------------------------------------
	private static void ConvertImage_Mutable( int imageHandle, ref MutableInstance.TEXTURE texData )
	{
		texData.sizeX = Mutable.mutable_get_image_size_x( imageHandle );
		MutableCommand.check_error();
		
		texData.sizeY = Mutable.mutable_get_image_size_y( imageHandle );
		MutableCommand.check_error();
		
		texData.mips = Mutable.mutable_get_image_lod_count( imageHandle );
		MutableCommand.check_error();
		
		texData.mutableFormat = Mutable.mutable_get_image_format( imageHandle );
		MutableCommand.check_error();		
		
		bool nativeSupport = Mutable.mutable_has_native_texture_copy_support()!=0;		
		if (nativeSupport)
		{
			texData.imageHandle = imageHandle;		
		}
		else
		{		
			// Slow path.
			// TODO: Optimize the copy
			
			int pixelCount = texData.sizeX*texData.sizeY;
			
			switch ( texData.mutableFormat )
			{
			case Mutable.IMAGE_FORMAT.IF_BGRA_UBYTE: 
			{
				byte[] srcPixels = new byte[pixelCount*4];
	
				Mutable.mutable_get_image_data_uint8( imageHandle, srcPixels, 0 );
				MutableCommand.check_error();
				
				// Copy pixels
				texData.pixels = new Color32[pixelCount];
				for (int p=0; p<pixelCount; ++p)
				{
					texData.pixels[p].r=srcPixels[p*4+2];
					texData.pixels[p].g=srcPixels[p*4+1];
					texData.pixels[p].b=srcPixels[p*4+0];
					texData.pixels[p].a=srcPixels[p*4+3];
				}
				break;
			}
				
			case Mutable.IMAGE_FORMAT.IF_RGBA_UBYTE: 
			{
				// Slow path.
				// TODO: Optimize the copy
				byte[] srcPixels = new byte[pixelCount*4];
	
				Mutable.mutable_get_image_data_uint8( imageHandle, srcPixels, 0 );
				MutableCommand.check_error();
				
				// Copy pixels
				texData.pixels = new Color32[pixelCount];
				for (int p=0; p<pixelCount; ++p)
				{
					texData.pixels[p].r=srcPixels[p*4+0];
					texData.pixels[p].g=srcPixels[p*4+1];
					texData.pixels[p].b=srcPixels[p*4+2];
					texData.pixels[p].a=srcPixels[p*4+3];
				}				
				break;
			}
				
			case Mutable.IMAGE_FORMAT.IF_RGB_UBYTE: 
			{
				byte[] srcPixels = new byte[pixelCount*3];
	
				Mutable.mutable_get_image_data_uint8( imageHandle, srcPixels, 0 );
				MutableCommand.check_error();
				
				// Copy pixels
				texData.pixels = new Color32[pixelCount];
				for (int p=0; p<pixelCount; ++p)
				{
					texData.pixels[p].r=srcPixels[p*3+0];
					texData.pixels[p].g=srcPixels[p*3+1];
					texData.pixels[p].b=srcPixels[p*3+2];
					texData.pixels[p].a=255;
				}
				break;
			}
					
			case Mutable.IMAGE_FORMAT.IF_L_UBYTE: 
			{
				byte[] srcPixels = new byte[pixelCount];
	
				Mutable.mutable_get_image_data_uint8( imageHandle, srcPixels, 0 );
				MutableCommand.check_error();
				
				// Copy pixels
				texData.pixels = new Color32[pixelCount];
				for (int p=0; p<pixelCount; ++p)
				{
					texData.pixels[p].r=srcPixels[p*3];
					texData.pixels[p].g=srcPixels[p*3];
					texData.pixels[p].b=srcPixels[p*3];
					texData.pixels[p].a=srcPixels[p*3];
				}
				break;
			}
				
			default: 
				break;
			}
		}		

	}	
	
}


//-------------------------------------------------------------------------------------------------
//! 
//-------------------------------------------------------------------------------------------------
[AddComponentMenu("Mutable/Mutable Manager")]
public class MutableManager : MonoBehaviour
{
	public static MutableManager s_instance = null;
	
	public bool workInBackgroundThread = false;
	
	// Background thread
	private System.Threading.Thread m_mutableThread;
	
	private Queue<MutableCommand> pendingCommands;
	private Queue<MutableCommand> finishedCommands;
	
	
	private void MutableThreadFunction()
    {
		MutableCommand.m_threadId = System.Threading.Thread.CurrentThread.ManagedThreadId;
		
		bool end = false;
		while( !end ) 
		{
		    MutableCommand com;
		    lock(pendingCommands) 
			{
		        while(pendingCommands.Count == 0) 
				{
					// releases lock, waits for a Pulse, and re-acquires the lock
		            System.Threading.Monitor.Wait(pendingCommands); 
		        }
				
				// we have the lock, and there's data				
		        com = pendingCommands.Dequeue(); 
				
				if ( com.type==MutableCommand.Type.UPDATE_INSTANCE )
				{
					// Mark the instance as busy until we complete the update.
					com.instance.updating = true;
				}
		    }
			
		    // process item outside of the lock of the add queue
			com.Execute();
			
		    lock(finishedCommands) 
			{
				finishedCommands.Enqueue( com );
				System.Threading.Monitor.PulseAll(finishedCommands);
			}
						
			if (com.type==MutableCommand.Type.RELEASE_SYSTEM)
			{
				end = true;
			}			
		}    
	}
	
	
	//!
	void Awake ()
	{			
		m_instances = new List<int>();
		m_parameters = new List<int>();
		m_pendingImagesToFree = new List<int>();		
		
		s_instance = this;
		
		if (!workInBackgroundThread)
		{
			MutableCommand.m_threadId = System.Threading.Thread.CurrentThread.ManagedThreadId;
		}
		else
		{
			pendingCommands = new Queue<MutableCommand>();
			finishedCommands = new Queue<MutableCommand>();
			m_mutableThread = new System.Threading.Thread( MutableThreadFunction );
			m_mutableThread.Name = "MutableThread";	
			m_mutableThread.Start();
		}
		
		// Initialize mutable
		MutableCommand initCommand = new MutableCommand();
		initCommand.type = MutableCommand.Type.INIT_SYSTEM;
		ExecuteCommand( initCommand, true );
				
	}
		
	// Do the Unity side of the instance update
	void CompleteInstanceUpdate( MutableInstance instance )
	{
		Trace.Log( "MutableManager.CompleteInstanceUpdate " );
		
		//
		Dictionary<uint,MutableInstance.MESH_UNITY> newIdToMesh = new Dictionary<uint,MutableInstance.MESH_UNITY>();
		Dictionary<uint,MutableInstance.TEXTURE_UNITY> newIdToImage = new Dictionary<uint,MutableInstance.TEXTURE_UNITY>();
		
		bool nativeRenderUpdateRequired = false;
		
		instance.componentsUnity = new MutableInstance.COMPONENT_UNITY[ instance.components.Length ];		
		for ( int c=0; c<instance.components.Length; ++c )
		{
			int meshCount = instance.components[c].meshes.Length;		
			
			instance.componentsUnity[c].meshes = new MutableInstance.MESH_UNITY[ meshCount ];		
			for ( int m=0; m<meshCount; ++m )
			{
				uint meshId = instance.components[c].meshes[m].meshId;
					
				// New mesh, not reused.
				if ( instance.components[c].meshes[m].reused )
				{
					instance.componentsUnity[c].meshes[m] = instance.m_idToMesh[ meshId ];
				}
				else
				{				
					Mesh mesh = ConvertMesh_Unity( instance.components[c].meshes[m] );					
					instance.componentsUnity[c].meshes[m].mesh = mesh;
					
					if (instance.m_model.m_referenceObjectBoneNames!=null
						&&
						instance.m_model.m_referenceObjectBoneNames.Length>0)
					{
						/*
						string bones = "New bones : \n";
						for ( int b=0; b<m_mutableInstance.components[0].meshes[0].bones.Length; ++b )
						{
							bones += m_mutableInstance.components[0].meshes[0].bones[b].name + "\n";
						}
						Debug.Log( bones );
				
						bones = "Old bones : \n";
						for ( int b=0; b<meshRenderer.bones.Length; ++b )
						{
							bones += meshRenderer.bones[b].name + " "+ meshRenderer.bones[b].gameObject.name + "\n";
						}
						Debug.Log( bones );
						*/
						
						int newBoneCount = instance.components[c].meshes[m].bones.Length;
						if ( newBoneCount>0 )
						{
							int[] bonesNewToOld = new int[ newBoneCount ];
							for ( int b=0; b<newBoneCount; ++b )
							{
								string newBoneName = instance.components[c].meshes[m].bones[b].name;
								
								int oldBone = -1;
								for ( int nb=0; nb<instance.m_model.m_referenceObjectBoneNames.Length; ++nb)
								{
									if ( instance.m_model.m_referenceObjectBoneNames[nb]==newBoneName )
									{
										oldBone = nb;
									}
								}
								
								if (oldBone<0)
								{
									Debug.LogWarning("Bone ["+instance.components[c].meshes[m].bones[b].name+"] not found in the reference mesh.");
									oldBone = 0;
								}
								
								bonesNewToOld[b] = oldBone;
								//Debug.Log( "map "+b+" to "+oldBone );
							}
							 						
							BoneWeight[] boneWeights = instance.components[c].meshes[m].boneWeights;
							for (int v=0; v<boneWeights.Length; ++v)
							{
								boneWeights[v].boneIndex0 = bonesNewToOld[ boneWeights[v].boneIndex0 ];
								boneWeights[v].boneIndex1 = bonesNewToOld[ boneWeights[v].boneIndex1 ];
								boneWeights[v].boneIndex2 = bonesNewToOld[ boneWeights[v].boneIndex2 ];
								boneWeights[v].boneIndex3 = bonesNewToOld[ boneWeights[v].boneIndex3 ];
							}
							mesh.boneWeights = boneWeights;
						}
						else
						{
							// No bones, for some reason.
							Debug.LogWarning("The mutable model doesn't have skin, but it is assigned to an object with SkinnedMeshRenderer. It wont's show properly.");
							
							BoneWeight[] boneWeights = mesh.boneWeights;

							for (int v=0; v<boneWeights.Length; ++v)
							{
								boneWeights[v].boneIndex0 = 0;
								boneWeights[v].boneIndex1 = 0;
								boneWeights[v].boneIndex2 = 0;
								boneWeights[v].boneIndex3 = 0;
							}
							
							mesh.boneWeights = boneWeights;
						}
						mesh.bindposes = instance.m_model.m_referenceObjectBindPoses;		
						
						//Debug.Log("Bind poses: "+oldMesh.bindposes.Length+" old, "+components[c].meshes[m].mesh.bindposes.Length+" new.");
					}
					
				}
				
				newIdToMesh[meshId] = instance.componentsUnity[c].meshes[m];
			}
			
			
			int surfaceCount = instance.components[c].materials.Length;
			instance.componentsUnity[c].materials = new MutableInstance.MATERIAL_UNITY[ surfaceCount ];		
			for ( int s=0; s<surfaceCount; ++s )
			{
				int imageCount = instance.components[c].materials[s].textures.Length;				
				instance.componentsUnity[c].materials[s].textures = new MutableInstance.TEXTURE_UNITY[ imageCount ];		

				for ( int i=0; i<imageCount; ++i )
				{	
					MutableInstance.TEXTURE texData = instance.components[c].materials[s].textures[i];
					
					uint imageId = texData.imageId;
										
					if (texData.reused)
					{
						instance.componentsUnity[c].materials[s].textures[i] = instance.m_idToImage[imageId];
					}
					else
					{																
						Texture2D image = null;

						// Native case
						if ( texData.pixels==null )
						{
							switch ( texData.mutableFormat )
							{
							case Mutable.IMAGE_FORMAT.IF_BGRA_UBYTE: 
							case Mutable.IMAGE_FORMAT.IF_RGBA_UBYTE: 
								image = new Texture2D( texData.sizeX, texData.sizeY, TextureFormat.ARGB32, texData.mips>1, true );
								break;
								
							case Mutable.IMAGE_FORMAT.IF_BC1: 
								image = new Texture2D( texData.sizeX, texData.sizeY, TextureFormat.DXT1, texData.mips>1, true );
								break;
								
							case Mutable.IMAGE_FORMAT.IF_BC3: 
								image = new Texture2D( texData.sizeX, texData.sizeY, TextureFormat.DXT5, texData.mips>1, true );
								break;
								
							case Mutable.IMAGE_FORMAT.IF_L_UBYTE: 
								image = new Texture2D( texData.sizeX, texData.sizeY, TextureFormat.Alpha8, texData.mips>1, true );
								break;
								
								
							// From here on the formats shouldn't really be used, for performance reasons. They will force a pixel conversion
							// operation. Warn somehow?
							case Mutable.IMAGE_FORMAT.IF_RGB_UBYTE: 
							case Mutable.IMAGE_FORMAT.IF_RGB_UBYTE_RLE: 
								image = new Texture2D( texData.sizeX, texData.sizeY, TextureFormat.RGB24, texData.mips>1, true );
								break;
												
							case Mutable.IMAGE_FORMAT.IF_RGBA_UBYTE_RLE: 
								image = new Texture2D( texData.sizeX, texData.sizeY, TextureFormat.ARGB32, texData.mips>1, true );
								break;
												
							case Mutable.IMAGE_FORMAT.IF_L_UBYTE_RLE: 
								image = new Texture2D( texData.sizeX, texData.sizeY, TextureFormat.Alpha8, texData.mips>1, true );
								break;
				
							default: 
					            Debug.LogWarning( "Unsupported native image format : " + texData.mutableFormat );
								Debug.DebugBreak();
								break;
							}
							
							if (image)
							{
								// This is thread-safe
								Mutable.mutable_copy_image( texData.imageHandle, image.GetNativeTexturePtr() );
								
								// Release the image handle
								MutableCommand com = new MutableCommand();
								com.type = MutableCommand.Type.RELEASE_IMAGE;
								com.manager = this;
								com.imageHandle = texData.imageHandle;
								ExecuteCommand( com, false );
								
								nativeRenderUpdateRequired = true;
							}
						}
						else
						{		
							// Slow path.
							switch ( texData.mutableFormat )
							{
							case Mutable.IMAGE_FORMAT.IF_BGRA_UBYTE: 
							{
								image = new Texture2D( texData.sizeX, texData.sizeY, TextureFormat.ARGB32, texData.mips>1 );
								image.SetPixels32( texData.pixels, 0 );
								image.Apply( true );
								break;
							}
								
							case Mutable.IMAGE_FORMAT.IF_RGBA_UBYTE: 
							{
								image = new Texture2D( texData.sizeX, texData.sizeY, TextureFormat.ARGB32, texData.mips>1 );
								image.SetPixels32( texData.pixels, 0 );
								image.Apply( true );
								break;
							}
								
							case Mutable.IMAGE_FORMAT.IF_RGB_UBYTE: 
							{
								image = new Texture2D( texData.sizeX, texData.sizeY, TextureFormat.ARGB32, texData.mips>1, true );
								image.SetPixels32( texData.pixels, 0 );		
								image.Apply( true );
								break;
							}
									
							case Mutable.IMAGE_FORMAT.IF_L_UBYTE: 
							{
								image = new Texture2D( texData.sizeX, texData.sizeY, TextureFormat.Alpha8, texData.mips>1 );
								image.SetPixels32( texData.pixels, 0 );
								image.Apply( true );
								break;
							}
								
							default: 
					            Debug.Log( "Unsupported image format : " + texData.mutableFormat );
								Debug.DebugBreak();
								break;
							}
						}
						
						instance.componentsUnity[c].materials[s].textures[i].texture = image;						
					}

					instance.componentsUnity[c].materials[s].textures[i].name = texData.name;

					newIdToImage[imageId] = instance.componentsUnity[c].materials[s].textures[i];					
				}			
			}
		}
		
		if (nativeRenderUpdateRequired)
		{								
			// Issue a plugin event with arbitrary integer identifier.
			// The plugin can distinguish between different
			// things it needs to do based on this ID.
			// For our plugin, it does not matter which ID we pass here.
			GL.IssuePluginEvent(1);
		}
					
		// Destroy resources that are not reused
		if (instance.m_idToMesh!=null)
		{		
			foreach(KeyValuePair<uint, MutableInstance.MESH_UNITY> entry in instance.m_idToMesh)
			{
			    if ( !newIdToMesh.ContainsValue(entry.Value) )
				{
					Mesh.DestroyImmediate( entry.Value.mesh );
				}
			}
			
			foreach(KeyValuePair<uint, MutableInstance.TEXTURE_UNITY> entry in instance.m_idToImage)
			{
			    if ( !newIdToImage.ContainsValue(entry.Value) )
				{
					Texture.DestroyImmediate( entry.Value.texture );
				}
			}
		}
		
		instance.m_idToMesh = newIdToMesh;
		instance.m_idToImage = newIdToImage;
		
		// Increment the version number to let the objects know
		instance.m_instanceVersion++;		
	}
	
	
	private void ProcessFinishedCommands()
	{
		if (workInBackgroundThread)
		{
			bool end = false;
			MutableCommand com;
			
			while (!end)
			{
				com = null;
		    	lock(finishedCommands) 
				{
			        if (finishedCommands.Count>0  ) 
					{
						// we have the lock, and there's data
				        com = finishedCommands.Dequeue(); 
					}
					else
					{
						end = true;
					}
				}
						
				// 
				if (com!=null)
				{
					if ( com.type==MutableCommand.Type.UPDATE_INSTANCE )
					{
						CompleteInstanceUpdate( com.instance );
						end = true;
						
						MutableCommand completedCom = new MutableCommand();
						completedCom.instance = com.instance;
						completedCom.type = MutableCommand.Type.COMPLETED_UPDATE_INSTANCE;
						ExecuteCommand(completedCom,false);
					}
					/* currently done in a synched way 
					else if ( com.type==MutableCommand.Type.IMAGE_UPLOAD )
					{
						// Issue a plugin event with arbitrary integer identifier.
						// The plugin can distinguish between different
						// things it needs to do based on this ID.
						// For our plugin, it does not matter which ID we pass here.
						GL.IssuePluginEvent(1);
					}
					*/
				}	
			}
		}
	}
		
	
	// Called once per frame
	void Update()
	{
		// Check for results of commands from the mutable thread
		ProcessFinishedCommands();
		
		// Check for data requests from the mutable thread
		
	}
	
	
	// Run a command either in this thread or in the mutable thread depending on the properties
	// of this manager.
	private void ExecuteCommand( MutableCommand com, bool synch )
	{
		if (workInBackgroundThread)
		{
		    lock(pendingCommands) 
			{
				bool delay = false;
								
				if (com.type==MutableCommand.Type.CANCEL_UPDATES)				
				{
					// Cancel unprocessed updates
					MutableCommand[] comArray = pendingCommands.ToArray();
					for (int c=0; c<comArray.Length; ++c)
					{
						if (comArray[c].type==MutableCommand.Type.UPDATE_INSTANCE
							&&
							(com.instance==null || com.instance==comArray[c].instance) )
						{
							comArray[c].cancelled = true;
						}
					}						
				}
				
				else if (com.type==MutableCommand.Type.UPDATE_INSTANCE)
				{					
					// Cancel other unprocessed updates of the same instance
					MutableCommand[] comArray = pendingCommands.ToArray();
					for (int c=0; c<comArray.Length; ++c)
					{
						if (comArray[c].type==MutableCommand.Type.UPDATE_INSTANCE
							&&
							comArray[c].instance==com.instance
							)
						{
							comArray[c].cancelled = true;
						}
					}					

					// See if we are already updating this instance, and queue it for later if needed
					if ( com.instance.updating )
					{
						com.instance.nextUpdate = com;
						delay = true;
	
						Trace.Log( "MutableCommand.ExecuteCommand: command delayed" );					
					}
				}
				
				else if (com.type==MutableCommand.Type.COMPLETED_UPDATE_INSTANCE)
				{
					com.instance.updating = false;
					
					// See if we have to recover a delayed update command
					if (com.instance.nextUpdate!=null)
					{
						// Enqueue the next update instead.
						com = com.instance.nextUpdate;
					}
					else
					{
						delay = true;
					}
				}
				
				
				if (!delay)
				{
					// Add this command to the queue
					pendingCommands.Enqueue( com );
					System.Threading.Monitor.PulseAll(pendingCommands);
				}
			}
			
			// Wait for this command to finish if necessary
			if (synch)
			{
			    lock(finishedCommands) 
				{
			        while(!finishedCommands.Contains(com)) 
					{
			            System.Threading.Monitor.Wait(finishedCommands); // releases lock, waits for a Pulse, and re-acquires the lock
			        }
					
					// We have the lock.
					// We don't need to do anything it will be dequeued automatically during update
			    }
				
				// We don't have the lock.
				// We don't need to do anything it will be dequeued automatically during update
			}
		}
		else
		{
			com.Execute();
			
			if (com.type==MutableCommand.Type.UPDATE_INSTANCE)
			{
				CompleteInstanceUpdate( com.instance );
			}
		}
	}

		
	//! Free all unused models.
	public void FreeUnusedModels()
	{		
		for( int m=0; m<transform.childCount; ++m )
		{
			MutableModel model = transform.GetChild(m).GetComponent<MutableModel>();			
			if ( model )
			{
	        	model.FreeUnused();
			}
		}
	}
	
	
	//  
	public void InitializeModel( MutableModel model )
	{
		if (model.MutableModelAsset!=null)
		{
			model.m_streamingPrefix = Application.streamingAssetsPath + "/" + model.ModelDataStreamingFolder + model.MutableModelAsset.name;

			MutableCommand command = new MutableCommand();
			command.type = MutableCommand.Type.INIT_MODEL;
			command.manager = this;
			command.model = model;
			command.modelBuffer = model.MutableModelAsset.bytes;

			ExecuteCommand( command, true );
		}
	}	
		
	
	//  
	public void BeginInstanceUpdate( MutableInstance instance )
	{
		MutableCommand command = new MutableCommand();
		command.type = MutableCommand.Type.BEGIN_UPDATE_INSTANCE;
		command.manager = this;
		command.instance = instance;
		ExecuteCommand( command, false );
	}

	
	//  
	public void InstanceUpdate( MutableInstance instance, bool synch )
	{
		Trace.Log( "MutableManager.InstanceUpdate " );
		MutableCommand command = new MutableCommand();
		command.type = MutableCommand.Type.UPDATE_INSTANCE;
		command.manager = this;
		command.instance = instance;
		ExecuteCommand( command, synch );
	}
	
	
	//
	public void EndInstanceUpdate( MutableInstance instance )
	{
 		MutableCommand command = new MutableCommand();
		command.type = MutableCommand.Type.END_UPDATE_INSTANCE;
		command.manager = this;
		command.instance = instance;
		ExecuteCommand( command, false );
	}
	
	
	// 
	public void ReleaseModel( MutableModel model )
	{
 		MutableCommand command = new MutableCommand();
		command.type = MutableCommand.Type.RELEASE_MODEL;
		command.manager = this;
		command.model = model;
		ExecuteCommand( command, true );
	}
			
		
	//!
	public static Mesh ConvertMesh_Unity( MutableInstance.MESH meshData )
	{
		Mesh mesh = new Mesh();		
		mesh.vertices = meshData.vertices;
		mesh.normals = meshData.normals;
		mesh.tangents = meshData.tangents;
		mesh.boneWeights = meshData.boneWeights;
		mesh.uv = meshData.uvs0;
		if (meshData.uvs1!=null)
		{
			mesh.uv2 = meshData.uvs1;
		}
				
		// Indices
		if (meshData.surfaces!=null)
		{
			mesh.subMeshCount = meshData.surfaces.Length;
			
			for (int s=0;s<meshData.surfaces.Length; ++s)
			{
				int[] subIndices = new int[meshData.surfaces[s].count];
				System.Array.Copy( meshData.indices, meshData.surfaces[s].firstIndex, subIndices, 0, meshData.surfaces[s].count);									
				mesh.SetTriangles( subIndices, s );
			}
		}
		else
		{								
			mesh.triangles = meshData.indices;
		}
				
		mesh.RecalculateBounds();
		
		return mesh;
	}
		
	
	//!
	void OnDestroy()
	{
		if (s_instance)
		{
			ReleaseMutable( true );
		}
	}
		
	
	public void ReleaseInstance( MutableInstance instance )
	{				
		// Cancel pending updates
		MutableCommand cancelCom = new MutableCommand();
		cancelCom.type = MutableCommand.Type.CANCEL_UPDATES;
		cancelCom.manager = this;
		cancelCom.instance = instance;
		ExecuteCommand( cancelCom, false );
	}
	
	
	void ReleaseMutable( bool cancelPending )
	{				
		// Cancel pending updates
		MutableCommand cancelCom = new MutableCommand();
		cancelCom.type = MutableCommand.Type.CANCEL_UPDATES;
		cancelCom.manager = this;
		ExecuteCommand( cancelCom, true );
		
		for( int m=0; m<transform.childCount; ++m )
		{
			MutableModel model = transform.GetChild(m).GetComponent<MutableModel>();			
			if ( model )
			{
				if (model.m_handle>=0)
				{
					ReleaseModel( model );
				}
			}
		}
		
		// Finalize mutable
		MutableCommand finCom = new MutableCommand();
		finCom.type = MutableCommand.Type.RELEASE_SYSTEM;
		finCom.manager = this;
		ExecuteCommand( finCom, true );
		
		if (m_mutableThread!=null)
		{
			m_mutableThread.Join();
		}

		s_instance = null;
	}
	
	
	[HideInInspector]
	public List<int> m_instances;

	[HideInInspector]
	public List<int> m_parameters;		
		
	// List of image handles that have not been freed because they are pending to be uploaded
	[HideInInspector]
	public List<int> m_pendingImagesToFree;
	
	
	
}


