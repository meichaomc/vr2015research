using UnityEngine;
using System.Collections;
using System;

public class MutableDemoMainGUI : MonoBehaviour {
	
	public Texture2D antictoIcon;
	
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	

	void OnGUI () {
		
		GUI.DrawTexture(new Rect(Screen.width - 256, 10, 256, 64), antictoIcon );
		
		GUI.Label(new Rect(Screen.width - 120, Screen.height-50, 120, 50), "demo version 0.7 http://anticto.com" );
		
		// Level switching
		if (GUI.Button (new Rect (10,Screen.height-40,60,30), "Quit")) 
		{
			Application.Quit();	
		}		
		
		string[] options = { "Character", "Helmet", "City" };
		string[] levels = { "Lobby", "Helmet", "City" };

		int val = Array.IndexOf(levels, Application.loadedLevelName );
		
		int newVal = GUI.SelectionGrid (new Rect (10, 10, 300, 40), val, options, options.Length );

		if ( newVal!=val )
		{
			Application.LoadLevel( levels[newVal] );
		}
		
	}

}
