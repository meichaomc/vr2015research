using UnityEngine;
using System.Collections;
 
[AddComponentMenu("Camera-Control/Mouse Orbit with zoom")]
public class CameraOrbit : MonoBehaviour {
 
    public Transform target;
    public float distance = 5.0f;
    public float xSpeed = 120.0f;
    public float ySpeed = 120.0f;
  
    public float distanceMin = .5f;
    public float distanceMax = 15f;
 
    float x = 0.0f;
    float y = 0.0f;
	
	private bool m_updated = false;
 
	// Use this for initialization
	void Start () 
	{
        Vector3 angles = transform.eulerAngles;
        x = angles.y;
        y = angles.x;
		
		m_updated = false;
 	}
 
    void LateUpdate () 
	{
	    if ( !m_updated 
			|| 
			(target && Input.GetMouseButton(0) && GUIUtility.hotControl==0) 
			|| 
			(target && Input.GetAxis("Mouse ScrollWheel")!=0.0f && GUIUtility.hotControl==0) 
			)
			{
			m_updated = true;
			
	        x += Input.GetAxis("Mouse X") * xSpeed * distance * 0.02f;
	        y -= Input.GetAxis("Mouse Y") * ySpeed * 0.02f;
	 	 
	        Quaternion rotation = Quaternion.Euler(y, x, 0);
	 
	        distance = Mathf.Clamp(distance - Input.GetAxis("Mouse ScrollWheel")*2, distanceMin, distanceMax);
	 
	        Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
	        Vector3 position = rotation * negDistance + target.position;
	 
	        transform.rotation = rotation;
	        transform.position = position;
	 
	    }	
	}
 
 
}
	
	