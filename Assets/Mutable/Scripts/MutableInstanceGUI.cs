using UnityEngine;
using System.Collections;

[AddComponentMenu("Mutable/Instance UI")]
public class MutableInstanceGUI : MonoBehaviour 
{//customized by chao on Jun 25 2014 with functionality of sychronize several GUI together
	public MutableInstance external;//must have exactly same structure with this one
	public bool useExternalGUI;
		
	private bool m_initialized = false;
	private bool needsUpdate = false;
	
	private bool showAllParameters = true;//cm on jun 25 false->true
			
	public Vector2 Position = new Vector2( 10, 10 );
	
	
	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (!m_initialized)
		{
			m_initialized = true;
			
			MutableInstance mutableInstance = GetComponent<MutableInstance>();		
			if ( mutableInstance )
			{
				mutableInstance.BeginMutableUpdate();
				needsUpdate=true;
			}
		}

		if ( needsUpdate )
		{
			needsUpdate = false;
			
			MutableInstance mutableInstance = GetComponent<MutableInstance>();
			
			if ( mutableInstance )
			{
				// Asynch update
				mutableInstance.MutableUpdate( false );
			}
		}
	}
	
	void OnDestroy()
	{
		MutableInstance mutableInstance = GetComponent<MutableInstance>();
		
		if ( mutableInstance )
		{
			mutableInstance.EndMutableUpdate();
		}
	}
	
	private int GetParamUIHeight( MutableInstance mutableInstance, int p )
	{
		int totalY = 0;
		
		switch ( mutableInstance.GetParameterType(p) )
		{
		case Mutable.PARAMETER_TYPE.T_FLOAT: totalY+=30; break;
		case Mutable.PARAMETER_TYPE.T_BOOL: totalY+=30; break;
		case Mutable.PARAMETER_TYPE.T_COLOUR: totalY+=80; break;
		case Mutable.PARAMETER_TYPE.T_INT:
			string[] options = mutableInstance.GetParameterIntOptions(p);
			totalY+=5+options.Length*25;
			break;
		default: totalY+=30; break;
		}
		
		return totalY;
	}
	
	private void AddParameterUI( MutableInstance mutableInstance, int p, int x, ref int y )
	{
		switch ( mutableInstance.GetParameterType(p) )
		{
		case Mutable.PARAMETER_TYPE.T_FLOAT:
		{
			int labelSize = 160;
			int controlX = labelSize + x + 5;
			GUI.Label(new Rect(x,y,labelSize,25), mutableInstance.GetParameterName(p));
		
			float val = mutableInstance.GetParameterValueFloat(p);
			float drawval;
			if(useExternalGUI){//add by cm on jun 25 to allow customize through other gui
				float externalVal = external.GetParameterValueFloat(p);
				drawval = externalVal;
			}else {
				drawval = val;
			}
			float newVal = GUI.HorizontalSlider( new Rect( controlX, y+5, 120, 20 ), drawval, 0.0f, 1.0f );
			if (val!=newVal)
			{
				mutableInstance.SetParameterValueFloat(p,newVal);
				needsUpdate = true;
			}
			y+=30;
			break;
		}
			
		case Mutable.PARAMETER_TYPE.T_BOOL:
		{
			int labelSize = 260;
			int controlX = labelSize + x + 5;
			GUI.Label(new Rect(x,y,labelSize,25), mutableInstance.GetParameterName(p));
		
			bool val = mutableInstance.GetParameterValueBool(p);
			bool drawval;
			if(useExternalGUI){//add by cm on jun 25 to allow customize through other gui
				bool externalVal = external.GetParameterValueBool(p);
				drawval = externalVal;
			}else {
				drawval = val;
			}
			bool newVal = GUI.Toggle( new Rect( controlX, y, 20, 20 ), drawval, "" );
			if (val!=newVal)
			{
				mutableInstance.SetParameterValueBool(p,newVal);
				needsUpdate = true;
			}
			y+=30;
			break;
		}
			
		case Mutable.PARAMETER_TYPE.T_COLOUR:
		{
			int labelSize = 160;
			int controlX = labelSize + x + 5;
			GUI.Label(new Rect(x,y,labelSize,25), mutableInstance.GetParameterName(p));
		
			Vector3 val = mutableInstance.GetParameterValueColour(p);				
			Vector3 newVal;
			
			GUI.Label( new Rect( controlX, y, 100, 20 ), "R" );
			newVal.x = GUI.HorizontalSlider( new Rect( controlX+20, y+5, 100, 20 ), val.x, 0.0f, 1.0f );
			y+=25;
			
			GUI.Label( new Rect( controlX, y, 100, 20 ), "G" );
			newVal.y = GUI.HorizontalSlider( new Rect( controlX+20, y+5, 100, 20 ), val.y, 0.0f, 1.0f );
			y+=25;
			
			GUI.Label( new Rect( controlX, y, 100, 20 ), "B" );
			newVal.z = GUI.HorizontalSlider( new Rect( controlX+20, y+5, 100, 20 ), val.z, 0.0f, 1.0f );
				
			if (val!=newVal )
			{
				mutableInstance.SetParameterValueColour(p,newVal);
				needsUpdate = true;
			}
			y+=30;
			break;
		}
			
		case Mutable.PARAMETER_TYPE.T_INT:
		{
			int labelSize = 160;
			int controlX = labelSize + x + 5;
			GUI.Label(new Rect(x,y,labelSize,25), mutableInstance.GetParameterName(p));
		
			int val = mutableInstance.GetParameterValueIntOption(p);
			string[] options = mutableInstance.GetParameterIntOptions(p);
			int drawval;
			string[] drawoptions;
			if(useExternalGUI){//add by cm on jun 25 to allow customize through other gui
				int externalVal = external.GetParameterValueIntOption(p);
				string[] externaloptions = external.GetParameterIntOptions(p);
				drawval = externalVal;
				drawoptions = externaloptions;
			}else {
				drawval = val;
				drawoptions = options;
			}
			int newVal = GUI.SelectionGrid (new Rect (controlX, y, 120, options.Length*25), drawval, drawoptions, 1);
			if (val!=newVal)
			{
				mutableInstance.SetParameterValueIntOption(p,newVal);
				needsUpdate = true;
			}
			y+=5+options.Length*25;
			break;
		}				
			
		default:
		{
			int labelSize = 200;
			int controlX = labelSize + x + 5;
			GUI.Label(new Rect(x,y,labelSize,25), mutableInstance.GetParameterName(p));
			
			GUI.Label( new Rect( controlX, y, 120, 20 ), "Unsupported type" );
			y+=30;
			break;
		}
			
		}
	}
	
	
	void OnGUI () 
	{				
		// See if the object that has this script also has a MutableComponent script
		MutableInstance mutableInstance = GetComponent<MutableInstance>();
		
		if ( !mutableInstance )
		{
			GUI.Box(new Rect(Position.x,Position.y,300,60), "");
			GUI.Label(new Rect(Position.x+5,Position.y+5,290,50), 
				"No Mutable Instance script was found in the object with this Mutable UI script.");
		}
		else
		{
			int state = mutableInstance.GetState();
			int stateParamCount = mutableInstance.GetStateParameterCount( state );			
			
			// Calculate necessary box size
			int totalY = 75;
			if ( !showAllParameters )
			{
				for( int stateParamIndex=0; stateParamIndex<stateParamCount; ++stateParamIndex )
				{
					int p = mutableInstance.GetStateParameterIndex( state, stateParamIndex );					
					totalY += GetParamUIHeight(mutableInstance, p);
				}
			}
			else
			{
				int paramCount = mutableInstance.GetParameterCount();			
				for( int p=0; p<paramCount; ++p )
				{
					totalY += GetParamUIHeight(mutableInstance, p);
				}
			}
			

			// Make a background box	
			Rect windowRect = new Rect(Position.x,Position.y,300,totalY);
			GUI.Box(windowRect, "");

			int x=(int)Position.x+5;
			int y=(int)Position.y+10;
			
			// State selection interface
			{
				int controlX = x + 5;
				
				string[] stateNames = mutableInstance.GetStateNames();
				int newState = GUI.SelectionGrid (new Rect (controlX, y, 280, 25), state, stateNames, stateNames.Length);
				if (state!=newState)
				{
					mutableInstance.SetState(newState);
					needsUpdate = true;
				}
				
				y+=30;
				
				showAllParameters = GUI.Toggle( new Rect( controlX, y, 280, 20 ), showAllParameters, " show all parameters" );
				y+=30;
			}
						
			if ( !showAllParameters )
			{
				// Parameters in the current state
				for( int stateParamIndex=0; stateParamIndex<stateParamCount; ++stateParamIndex )
				{
					int p = mutableInstance.GetStateParameterIndex( state, stateParamIndex );
					AddParameterUI( mutableInstance, p, x, ref y );
				}
			}
			else
			{
				int paramCount = mutableInstance.GetParameterCount();			
				for( int p=0; p<paramCount; ++p )
				{
					AddParameterUI( mutableInstance, p, x, ref y );
				}
			}
			
		}
		
	}
	
}
