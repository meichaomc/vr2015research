/*
 * ------------------------------------------------------------------------------------------------
 * (c) Anticto Estudi Binari S.L. This file is subject to the terms and conditions defined in 
 * the file 'license.txt', which is part of this package.
 * ------------------------------------------------------------------------------------------------
 */

using UnityEngine;

using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;


// This class stores the values for the parameters of a Mutable instance. It just for data storage
// and totally decoupled from other Mutable classes. This can be used to serialize the description 
// of a customized object to disk or over the network.
// This parameter description does not include any identifer of what Mutable model it should be
// used with.
// The parameter values stored here should only be manipulated through MutableInstance objects.
public class MutableParameters
{
	public struct BOOL_PARAMETER
	{
		public string name;
		public bool boolValue;
	};
	
	public BOOL_PARAMETER[] bools;
	
	public struct FLOAT_PARAMETER
	{
		public string name;
		public float floatValue;
	};
		
	public FLOAT_PARAMETER[] floats;

	public struct COLOUR_PARAMETER
	{
		public string name;
		public Vector3 colourValue;
	};
		
	public COLOUR_PARAMETER[] colours;

	public struct INT_PARAMETER
	{
		public string name;
		public int intValue;
	};
	
	public INT_PARAMETER[] ints;
	
	
	// Look for a bool value. Return true if found and set the out parameter v.
	public bool GetBoolValue( string name, out bool v )
	{
		foreach(var p in bools)
		{
			if (p.name==name)
			{
				v=p.boolValue;
				return true;
			}
		}
		
		v=false;
		return false;
	}
	
	//
	public bool SetBoolValue( string name, bool v )
	{
		for( int i=0; 0<bools.Length; ++i )
		{
			if (bools[i].name==name)
			{
				bools[i].boolValue=v;
				return true;
			}
		}
		
		return false;
	}

	// Look for a float value. Return true if found and set the out parameter v.
	public bool GetFloatValue( string name, out float v )
	{
		foreach(var p in floats)
		{
			if (p.name==name)
			{
				v=p.floatValue;
				return true;
			}
		}
		
		v=0.0f;
		return false;
	}
	
	//
	public bool SetFloatValue( string name, float v )
	{
		for( int i=0; 0<floats.Length; ++i )
		{
			if (floats[i].name==name)
			{
				floats[i].floatValue=v;
				return true;
			}
		}
		
		return false;
	}

	// Look for a colour value. Return true if found and set the out parameter v.
	public bool GetColourValue( string name, out Vector3 v )
	{
		foreach(var p in colours)
		{
			if (p.name==name)
			{
				v=p.colourValue;
				return true;
			}
		}
		
		v=new Vector3(0.0f,0.0f,0.0f);
		return false;
	}
	
	//
	public bool SetColourValue( string name, Vector3 v )
	{
		for( int i=0; 0<colours.Length; ++i )
		{
			if (colours[i].name==name)
			{
				colours[i].colourValue=v;
				return true;
			}
		}
		
		return false;
	}

	// Look for a int value. Return true if found and set the out parameter v.
	public bool GetIntValue( string name, out int v )
	{
		foreach(var p in ints)
		{
			if (p.name==name)
			{
				v=p.intValue;
				return true;
			}
		}
		
		v=0;
		return false;
	}
	
	//
	public bool SetIntValue( string name, int v )
	{
		for( int i=0; 0<ints.Length; ++i )
		{
			if (ints[i].name==name)
			{
				ints[i].intValue=v;
				return true;
			}
		}
		
		return false;
	}
	
	
	// Custom deep-clone
    public MutableParameters Clone()
    {
        MutableParameters other = new MutableParameters();
		other.bools = (MutableParameters.BOOL_PARAMETER[])bools.Clone();
		other.floats = (MutableParameters.FLOAT_PARAMETER[])floats.Clone();
		other.colours = (MutableParameters.COLOUR_PARAMETER[])colours.Clone();
		other.ints = (MutableParameters.INT_PARAMETER[])ints.Clone();		
		return other;
    }	
}


//
[AddComponentMenu("Mutable/Mutable Instance")]
public class MutableInstance : MonoBehaviour
{
	
	// These members should only be accessed from the manager, when the lock on the pending 
	// commands is acquired.
	[HideInInspector]
	public bool updating = false;

	[HideInInspector]
	public MutableCommand nextUpdate = null;
	
	
	// Currently built instance resources, accessed in controleld way from the mutable thread and unity thread.
	// Don't touch from outside the mutable scripts.
	public struct TEXTURE
	{
		public string name;
				
		// Mutable ID
		public volatile uint imageId;
		public volatile bool reused;
		
		public volatile int sizeX;
		public volatile int sizeY;
		public volatile int mips;
		public volatile Mutable.IMAGE_FORMAT mutableFormat;
		public volatile Color32[] pixels;		
		public volatile int imageHandle;
	};
	
	public struct MATERIAL
	{
		public volatile string name;
		public volatile TEXTURE[] textures;
	};
	
	public struct BONE
	{
		public volatile string name;
		//public Matrix4x4 bindPose;
	};
	
	public struct MESH
	{
		public volatile string name;
		public volatile BONE[] bones;
		
		// Mutable ID
		public volatile uint meshId;
		public volatile bool reused;
		
		public volatile BoneWeight[] boneWeights;
		public volatile Vector3[] vertices;
		public volatile Vector3[] normals;
		public volatile Vector4[] tangents;
		public volatile Vector2[] uvs0;
		public volatile Vector2[] uvs1;

		public volatile int[] indices;	
		public volatile int subMeshCount;
		public struct SURFACE
		{
			public volatile int firstIndex ;
			public volatile int count;
		}
		public volatile SURFACE[] surfaces;
	};
	
	public struct COMPONENT
	{
		public volatile string name;
		public volatile MESH[] meshes;
		public volatile MATERIAL[] materials;
	};
	
	[HideInInspector]
	public volatile COMPONENT[] components;
		
	// Unity converted resources, accessible from the main thread only. This makes them safe to be accessed from
	// mutable objects as well.
	public struct TEXTURE_UNITY
	{
		public string name;
		public Texture2D texture;
	};
	
	public struct MATERIAL_UNITY
	{
		public string name;
		public TEXTURE_UNITY[] textures;
	};
	
	public struct MESH_UNITY
	{
		public Mesh mesh;		
	};
	
	public struct COMPONENT_UNITY
	{
		public string name;
		public MESH_UNITY[] meshes;
		public MATERIAL_UNITY[] materials;
	};
	
	[HideInInspector]
	public COMPONENT_UNITY[] componentsUnity;
	

		
	// Current parameter values

	//! Parameter values
	[HideInInspector]
	public MutableParameters parameters;	
	

	
	//! Unity MonoBehaviour interface
	void Start ()
	{
		InternalInitialize();
	}
	
	
	//!
	public int GetVersion()
	{
		return m_instanceVersion;
	}
	
			
	//!
	public void BeginMutableUpdate()
	{
		InternalInitialize();
		
		MutableManager manager = GetManager();		
		if (m_model!=null && manager!=null)
		{
			m_updating = true;
			
			manager.BeginInstanceUpdate( this );			
		}
	}
	
	
	//!
	public void MutableUpdate( bool synch )
	{			
		// Set the parameter values 
		MutableManager manager = GetManager();
		
		if (manager)
		{
			manager.InstanceUpdate( this, synch );
		}
	}


	//!
	public void EndMutableUpdate()
	{		
		if (m_updating)
		{
			if (m_model!=null)
			{
				MutableManager manager = m_model.GetManager();
				if (manager!=null)
				{			
					manager.EndInstanceUpdate( this );
				}
			}

			m_updating = false;
			m_idToMesh = null;
			m_idToImage = null;
		}
	}


	//!
	public void onDestroy()
	{
		if (m_updating)
		{
			EndMutableUpdate();
		}
		
		MutableManager manager = m_model.GetManager();
		if (manager!=null)
		{			
			manager.ReleaseInstance( this );
		}
	}	
	
	
	// Get the current model state for this object. This refers to the States defined in the Mutable Tool when creating the model.
	public int GetState()
	{
		return state;
	}

    // Set the current model state for this object. This refers to the States defined in the Mutable Tool when creating the model.
    public void SetState(int s)
	{
		state = s;
	}


    // Get the number of model states available in this object
    public int GetStateCount()
	{
		InternalInitialize();
		
		if (m_model!=null)
		{
			return m_model.GetStateCount();
		}
		return 0;
	}


    // Get the names of the model states in this object
    public string[] GetStateNames()
	{
		InternalInitialize();
		
		if (m_model)
		{
			return m_model.GetStateNames();
		}
		
		return new string[0];
	}


    // Get the index of a state from its name. Return -1 if not found.
    public int FindState( string name )
	{
		InternalInitialize();
		
		int result = -1;

		if (m_model!=null)
		{
			return m_model.FindState(name);
		}

		return result;
	}
		
	
	// Set random values to the parameters
	public void GenerateRandom()
	{
		InternalInitialize();
		
		if (parameters!=null)
		{		
			for( int p=0; p<parameters.bools.Length; ++p )
			{
				parameters.bools[p].boolValue = Random.value>0.5f;
			}
					
			for( int p=0; p<parameters.floats.Length; ++p )
			{
				parameters.floats[p].floatValue = Random.value;
			}
					
			for( int p=0; p<parameters.colours.Length; ++p )
			{
				parameters.colours[p].colourValue.x = Random.value;
				parameters.colours[p].colourValue.y = Random.value;
				parameters.colours[p].colourValue.z = Random.value;
			}
					
			for( int p=0; p<parameters.ints.Length; ++p )
			{
				int paramDescIndex = -1;
				for( int i=0; i<m_model.parameterDescriptions.Length; ++i )
				{
					if (m_model.parameterDescriptions[i].name==parameters.ints[p].name
						&&
						m_model.parameterDescriptions[i].type==Mutable.PARAMETER_TYPE.T_INT)
					{
						paramDescIndex = i;
					}
				}
				
				int numValues = m_model.parameterDescriptions[paramDescIndex].intValues.Length;
				int valueIndex = (int) Mathf.Min( numValues-1, Random.value*numValues );
				parameters.ints[p].intValue = m_model.parameterDescriptions[paramDescIndex].intValues[valueIndex];
			}
		}
	}
	
	
    // Get the number of parameters that are exposed in the provided model state. 
	public int GetStateParameterCount( int state )
	{
		InternalInitialize();
		
		if (m_model!=null)
		{
			return m_model.GetStateParameterCount( state );
		}
			
		return 0;
	}


    // Get the index of one of parameters that are exposed in the provided model state. 
    // "param" goes from 0 to GetStateParameterCount(state)-1
    // The returned index refers to the list of all parameters in the model, so it wil have a
    // value between 0 and GetParameterCount().
    public int GetStateParameterIndex(int state, int param)
	{
		InternalInitialize();
		
		if (m_model!=null)
		{
			return m_model.GetStateParameterIndex(state,param);
		}

		return 0;
	}
	
	
	// Get the total number of parameters in the model.
	public int GetParameterCount()
	{
		InternalInitialize();
		
		if (m_model!=null)
		{
			return m_model.GetParameterCount();
		}

		return 0;
	}
	
	// Get the name of a parameter of the model.
	public string GetParameterName( int p )
	{
		InternalInitialize();
		
		if (m_model!=null)
		{
			return m_model.GetParameterName(p);
		}
		
		return "";
	}
	
	// Get the type of a parameter in the model
	public Mutable.PARAMETER_TYPE GetParameterType( int p )
	{
		InternalInitialize();
		
		if (m_model!=null)
		{
			return m_model.GetParameterType(p);
		}
		return Mutable.PARAMETER_TYPE.T_NONE;
	}
	
    // Get the floating-point value of a parameter of the model. The parameter at the specified
    // index must be a of type Mutable.PARAMETER_TYPE.T_FLOAT.
	public float GetParameterValueFloat( int p )
	{
		float result;
		bool found = parameters.GetFloatValue( GetParameterName(p), out result );
		if (!found)
		{
			Debug.LogError("Internal error: the parameter was not found.");
			Debug.Break();
		}
		return result;
	}

    // Set the floating-point value of a parameter of the model. The parameter at the specified
    // index must be a of type Mutable.PARAMETER_TYPE.T_FLOAT.
    public void SetParameterValueFloat(int p, float v)
	{
		bool found = parameters.SetFloatValue( GetParameterName(p), v );
		if (!found)
		{
			Debug.LogError("Internal error: the parameter was not found.");
			Debug.Break();
		}
	}

    // Get the boolean value of a parameter of the model. The parameter at the specified
    // index must be a of type Mutable.PARAMETER_TYPE.T_BOOL.
    public bool GetParameterValueBool(int p)
	{
		bool result;
		bool found = parameters.GetBoolValue( GetParameterName(p), out result );
		if (!found)
		{
			Debug.LogError("Internal error: the parameter was not found.");
			Debug.Break();
		}
		return result;
	}

    // Set the boolean value of a parameter of the model. The parameter at the specified
    // index must be a of type Mutable.PARAMETER_TYPE.T_BOOL.
    public void SetParameterValueBool(int p, bool v)
	{
		bool found = parameters.SetBoolValue( GetParameterName(p), v );
		if (!found)
		{
			Debug.LogError("Internal error: the parameter was not found.");
			Debug.Break();
		}
	}

    // Get the colour value of a parameter of the model. The parameter at the specified
    // index must be a of type Mutable.PARAMETER_TYPE.T_COLOUR.
    public Vector3 GetParameterValueColour(int p)
	{
		Vector3 result;
		bool found = parameters.GetColourValue( GetParameterName(p), out result );
		if (!found)
		{
			Debug.LogError("Internal error: the parameter was not found.");
			Debug.Break();
		}
		return result;
	}

    // Set the colour value of a parameter of the model. The parameter at the specified
    // index must be a of type Mutable.PARAMETER_TYPE.T_COLOUR.
    public void SetParameterValueColour(int p, Vector3 v)
	{
		bool found = parameters.SetColourValue( GetParameterName(p), v );
		if (!found)
		{
			Debug.LogError("Internal error: the parameter was not found.");
			Debug.Break();
		}
	}

    // Get the integer value of a parameter of the model. The parameter at the specified
    // index must be a of type Mutable.PARAMETER_TYPE.T_INT.
    public int GetParameterValueIntOption(int p)
	{
		if (m_model!=null)
		{
			int intValue = 0;
			bool found = parameters.GetIntValue( GetParameterName(p), out intValue );
			if (!found)
			{
				Debug.LogError("Internal error: the parameter was not found.");
				Debug.Break();
			}		
			
			for ( int o=0; o<m_model.parameterDescriptions[p].intValues.Length; ++o )
			{
				if ( m_model.parameterDescriptions[p].intValues[o] == intValue)
				{
					return o;
				}
			}
		}
		return 0;
	}

    // Set the integer value of a parameter of the model. The parameter at the specified
    // index must be a of type Mutable.PARAMETER_TYPE.T_INT.
    public void SetParameterValueIntOption(int p, int v)
	{
		if (m_model!=null)
		{
			int realValue;
			if (v<m_model.parameterDescriptions[p].intValues.Length)
			{
				realValue = m_model.parameterDescriptions[p].intValues[v];
			}
			else
			{			
				realValue = v;
			}
			
			bool found = parameters.SetIntValue( GetParameterName(p), realValue );
			if (!found)
			{
				Debug.LogError("Internal error: the parameter was not found.");
				Debug.Break();
			}
		}
		
	}

    // Get the names of possible integer value of a parameter of the model. 
    // The parameter at the specified index must be a of type Mutable.PARAMETER_TYPE.T_INT.
    public string[] GetParameterIntOptions(int p)
	{
		if (m_model!=null)
		{
			return m_model.parameterDescriptions[p].intOptions;
		}
		
		return new string[0];
	}
	
		
	
	//---------------------------------------------------------------------------------------------
	// Internal interface
	//---------------------------------------------------------------------------------------------
	
	// Cached reference to the parent model.
	[HideInInspector]
	public MutableModel m_model;

	//
	private bool m_updating = false;

	//
	[HideInInspector]
	public float m_importScaleFactor = 1.0f;

	// Incremented every time the mutable object is updated. It is observed by 
	// the Mutable Objects to find out if they need to update
	[HideInInspector]
	public int m_instanceVersion = -1;
	
	// Mutable ids for the resources built in the last operation
	[HideInInspector]
	public Dictionary<uint,MESH_UNITY> m_idToMesh;

	[HideInInspector]
	public Dictionary<uint,TEXTURE_UNITY> m_idToImage;
	
	// Mutable handles for objects used during Mutable operations
	[HideInInspector]
	public int m_modelHandle = -1;
	[HideInInspector]
	public int m_instance = -1;
	[HideInInspector]
	public int m_parameters = -1;
	
	// Current model state
	[HideInInspector]
	public int state = 0;	
	
	
	private void InternalInitialize()
	{
		if (m_model==null)
		{
			// Get the initial parameters
			m_model = GetModel();
			
			if (m_model)
			{
				// Convert the parameter values
				parameters = m_model.GetDefaultParameterValues().Clone();
			}
		}
	}

	
	//! Shortcut to the model
	private MutableModel GetModel()
	{
		if (transform && transform.parent)
		{
			return transform.parent.GetComponent<MutableModel>();
		}
		
		return null;
	}
		
	
	// Shortcut to the manager
	private MutableManager GetManager()
	{
		MutableModel model = GetModel();
		if (model)
		{
			return model.GetManager();
		}
		
		return null;
	}
	
	
}

