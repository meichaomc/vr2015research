using UnityEngine;
using System.Collections;

[AddComponentMenu("Mutable/Instance Camera")]
public class MutableInstanceCamera : MonoBehaviour 
{
	
	public GameObject Instance;
	public GameObject[] targets;
	
	private int m_lastState = -1;
	private Transform m_from;
	private Transform m_to;
	
	private MutableInstance m_mutableInstance;
	
	
	// Use this for initialization
	void Start () 
	{
		if ( Instance==null )
		{
			Debug.LogWarning("The Mutable Instance Camera script has no target instance assigned. It will be disabled.");
		}
		else		
		{
			if ( targets.Length==0 )
			{
				Debug.LogWarning("The Mutable Instance Camera script has no target cameras assigned. It will be disabled.");
			}

			m_mutableInstance = Instance.GetComponentInChildren<MutableInstance>();			
			if ( m_mutableInstance==null )
			{
				// Is it an object?
				MutableObject mutableObject = Instance.GetComponentInChildren<MutableObject>();	
				if (mutableObject)
				{
					if (mutableObject.Instance)
					{
						m_mutableInstance = mutableObject.Instance.GetComponentInChildren<MutableInstance>();					
					}
				}
				
				if ( m_mutableInstance==null )
				{
					Debug.LogWarning("The Mutable Instance Camera script target has no Mutable Instance component. It will be disabled.");
				}
			}
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		if ( m_mutableInstance && targets.Length>0 )
		{
			int state = m_mutableInstance.GetState();
			if (state!=m_lastState)
			{
				int targetIndex = state<targets.Length ? state : 0;
				
				if ( targets[targetIndex] )
				{
					m_from = GetComponent<Transform>();
					m_to = targets[targetIndex].GetComponent<Transform>();
				}
			}
			
			if ( m_from!=null && m_to!=null )
			{
				float factor = 0.1f;
				m_from.position = Vector3.Lerp( m_from.position, m_to.position, factor );
				m_from.rotation = Quaternion.Slerp( m_from.rotation, m_to.rotation, factor );
			}
		}		
	}
}
