using UnityEngine;

using System.IO;
using System.Runtime.InteropServices;


//-------------------------------------------------------------------------------------------------
//! 
//-------------------------------------------------------------------------------------------------
// TODO. Right now it is to messy to manage all resources 
//[ExecuteInEditMode]
[AddComponentMenu("Mutable/Mutable Object")]
public class MutableObject : MonoBehaviour
{	
	private const string MUTABLE_TEMP_RESOURCE_NAME = "mutable_temp_resource_tag";

	//
	public MutableInstance Instance = null;
	
	private int m_lastInstanceVersion = -1;

	//
	void Start ()
	{
		if ( Instance==null )
		{
			Debug.LogError("A Mutable Object component doesn't have a Mutable Instance set. It won't be customized.");
		}
	} 	
	
	
	// Update is called once per frame
	void LateUpdate ()
	{
		// If the instance has changed
		if ( Instance  )
		{
			MutableInstance mutableInstance = Instance.GetComponent<MutableInstance>();
			if ( mutableInstance
				&& 
				m_lastInstanceVersion != mutableInstance.GetVersion() )
			{
				m_lastInstanceVersion = mutableInstance.GetVersion();
				MutableUpdate( mutableInstance );
			}
		}
	}
			
	
    // 
	private void MutableUpdate( MutableInstance mutableInstance )
	{		
		//Debug.Log( "MutableObject.MutableUpdate v: "+m_lastInstanceVersion );
	
		// Non-skinned object?
		MeshFilter meshFilter = GetComponent<MeshFilter>();
		if (meshFilter)
		{
			if ( mutableInstance.componentsUnity.Length==0
				||
				mutableInstance.componentsUnity[0].meshes.Length==0
				)
			{
				Debug.LogWarning( "Mutable Instance didn't generate any mesh. Check your Mutable model." );
			}
			else if ( mutableInstance.componentsUnity.Length>1 )
			{
				Debug.LogWarning( "Mutable Instance has more than one component. Only the first one will be used in Unity." );
			}
			else
			{
				
				meshFilter.mesh = mutableInstance.componentsUnity[0].meshes[0].mesh;
			
				MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
				if (meshRenderer)
				{
					int matCountMutable = mutableInstance.componentsUnity[0].materials.Length;
					int matCountUnity = meshRenderer.sharedMaterials.Length;
					
					if (matCountMutable!=matCountUnity)
					{
						Debug.LogWarning("The number of materials in the reference objects is different than the Mutable object. Some materials may now show properly.");
					}
					
					int matCount = Mathf.Min( matCountUnity, matCountMutable );
					//meshRenderer.materials = new Material[ matCount ];
					
					for ( int m=0; m<matCount; ++m )
					{
						// Clone the material if necessary
						if ( meshRenderer.materials[m].name!=MUTABLE_TEMP_RESOURCE_NAME )
						{
							meshRenderer.materials[m] = (Material)Object.Instantiate(meshRenderer.sharedMaterials[m]);
							meshRenderer.materials[m].name = MUTABLE_TEMP_RESOURCE_NAME;
						}
						
						
						for ( int t=0; t<mutableInstance.componentsUnity[0].materials[m].textures.Length; ++t )
						{
							// Here we assign the textures from Mutable to the Unity material textures
							// This may need to be adapted to your material needs, for now it just uses the texture name.
							string texName = mutableInstance.componentsUnity[0].materials[m].textures[t].name;
							Texture tex = mutableInstance.componentsUnity[0].materials[m].textures[t].texture;
							tex.name = MUTABLE_TEMP_RESOURCE_NAME;
							
							meshRenderer.materials[m].SetTexture( texName, tex );
							
							if ( !meshRenderer.materials[m].HasProperty(texName) )
							{
								Debug.LogWarning("A texture ["+texName+"] was assigned to a material ["+meshRenderer.materials[m].name+"] that doesn't support it.");
							}
						}
					}
					
				}
				else
				{
					Debug.LogWarning( "Could not find a MeshRenderer component in object with a MutableComponent." );
				}
			}
		}

		// Skinned object?
		else
		{
			SkinnedMeshRenderer meshRenderer = GetComponentInChildren<SkinnedMeshRenderer>();
			if ( meshRenderer )
			{				
				if ( mutableInstance.componentsUnity==null
					||
					mutableInstance.componentsUnity.Length==0
					||
					mutableInstance.componentsUnity[0].meshes.Length==0
					)
				{
					Debug.LogWarning( "Mutable Instance didn't generate any mesh. Check your Mutable model." );
				}
				else if ( mutableInstance.componentsUnity.Length>1 )
				{
					Debug.LogWarning( "Mutable Instance has more than one component. Only the first one will be used in Unity." );
				}
				else
				{
					meshRenderer.sharedMesh = mutableInstance.componentsUnity[0].meshes[0].mesh;
					
								
					int matCountMutable = mutableInstance.componentsUnity[0].materials.Length;
					int matCountUnity = meshRenderer.sharedMaterials.Length;
					
					if (matCountMutable!=matCountUnity)
					{
						Debug.LogWarning("The number of materials in the reference objects is different than the Mutable object. Some materials may now show properly.");
					}
					
					int matCount = Mathf.Min( matCountUnity, matCountMutable );
					//meshRenderer.materials = new Material[ matCount ];
					
					for ( int m=0; m<matCount; ++m )
					{
						// Clone the material if necessary
						if ( meshRenderer.materials[m].name!=MUTABLE_TEMP_RESOURCE_NAME )
						{
							meshRenderer.materials[m] = (Material)Object.Instantiate(meshRenderer.sharedMaterials[m]);
							meshRenderer.materials[m].name = MUTABLE_TEMP_RESOURCE_NAME;
						}
						
						for ( int t=0; t<mutableInstance.componentsUnity[0].materials[m].textures.Length; ++t )
						{
							// Here we assign the textures from Mutable to the Unity material textures
							// This may need to be adapted to your material needs, for now it just uses the texture name.
							string texName = mutableInstance.componentsUnity[0].materials[m].textures[t].name;
							Texture tex = mutableInstance.componentsUnity[0].materials[m].textures[t].texture;
							tex.name = MUTABLE_TEMP_RESOURCE_NAME;
							
							meshRenderer.materials[m].SetTexture( texName, tex );
							
							if ( !meshRenderer.materials[m].HasProperty(texName) )
							{
								Debug.LogWarning("A texture ["+texName+"] was assigned to a material ["+meshRenderer.materials[m].name+"] that doesn't support it.");
							}
						}
					}
				}
			}
			
			else
			{
				Debug.LogWarning("A MutableObject script is assigned to an object without MeshRenderer so it won't do anything.\n" +
					"Please check that you have assigned to script to the right node.");
			}
			
		}
		
	}
	
	
}

