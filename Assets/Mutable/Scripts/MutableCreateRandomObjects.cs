﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// This script randomly creates objects with their own mutable instance in a specific area.
// The objects may stay for some time and then disappear.
[AddComponentMenu("Mutable/Create Random Objects")]
public class MutableCreateRandomObjects : MonoBehaviour 
{
	
	// Area where the objects will be created wit a randomized position and orientation.
	public BoxCollider area;
	
	// Object used in place of the final object while it is being created 
	public Transform placeHolder;
	
	// Mutable models to use to create the instances.
	public MutableModel[] models;
	
	// Number of objects that will be created at start of the level.
	public int initialObjects = 8;
	
	// Number of objects that will be created and removed every second.
	public float dynamicObjectsPerSecond = 0.5f;
	
	// This is used to accumulate the fraction of objects to be created. When larger than one.
	// the next update will create an object.
	private float pendingObjectsCreate = 0.0f;
	
	// This is used to accumulate the fraction of objects to be created. When larger than one.
	// the next update will remove an object. 
	private float pendingObjectsDelete = 0.0f;
	
	// Live objects
	private List<GameObject> citizens;
	
	// Use this for initialization
	void Start ()
	{
		citizens = new List<GameObject>();

		// Initialize with an offset to decouple creation and removal.
		pendingObjectsDelete = dynamicObjectsPerSecond*0.3f;
		
		if (placeHolder && area)
		{
			for (int c = 0; c<initialObjects; c++) 
			{
				// The initial population will be created synchronously.
				CreateRandomObject( true );
			}
		}
		
	}
	
	// Update is called once per frame
	void Update() 
	{		
		// Add the pending creation and destructions
		pendingObjectsCreate += dynamicObjectsPerSecond*Time.deltaTime;
		pendingObjectsDelete += dynamicObjectsPerSecond*Time.deltaTime;
		
		while (pendingObjectsCreate>1.0f)
		{
			pendingObjectsCreate-=1.0f;
			
			// Extra objects are created without locking the main thread
			CreateRandomObject( false );
		}
		
		while (pendingObjectsDelete>1.0f)
		{
			pendingObjectsDelete-=1.0f;
						
			// Destroy a random citizen
			if (citizens.Count>0)
			{
				int pos = ((int)(Random.value*citizens.Count)) % citizens.Count;
				GameObject citizen = citizens[pos];
				citizens.Remove( citizen );
				
				MutableObject mutableObject = citizen.GetComponent<MutableObject>();
				Object.Destroy( mutableObject.Instance.gameObject );
				Object.Destroy( citizen );
				
			}
		}
	}
	
	
	//
	void CreateRandomObject( bool synch )
	{
		Vector3 position = area.transform.position - area.size * 0.5f;
		position.x += Random.value * area.size.x;
		position.z += Random.value * area.size.z;
		
		Quaternion orientation = Quaternion.AngleAxis( 360.0f* Random.value, new Vector3(0, 1, 0) );
		
		// Create a new citizen object
		Transform citizen = (Transform)Instantiate(placeHolder, position, orientation );
		citizen.parent = this.transform;
		
		// Create a customizable instance or this citizen. It will be unique for every citizen.
		MutableModel model = models[0];
		GameObject instanceGameObject = new GameObject( "Random Mutable Instance" );
		instanceGameObject.transform.parent = model.transform;
		MutableInstance mutableInstance = (MutableInstance)instanceGameObject.AddComponent( "MutableInstance" );
		
		// This will set random values to the parameters.
		mutableInstance.GenerateRandom();
		
		// TODO: Investigate this. Using an editor-specified reference object prefab seems to fail beacuse there is no 
		// skinned mesh renderer in it.
		model.SetReferenceObject( citizen.gameObject );
				
		// Create the model object component using this instance
		MutableObject mutableObject = (MutableObject)citizen.gameObject.AddComponent("MutableObject");
		mutableObject.Instance = mutableInstance;
		
		// Update it
		int inGameStateIndex = Mathf.Max (0, mutableInstance.FindState("In-Game") );
		mutableInstance.SetState( inGameStateIndex );
		
		// This will build the instance asynchronoulsy (without locking the unity thread)
		mutableInstance.BeginMutableUpdate();
		mutableInstance.MutableUpdate( synch );
		mutableInstance.EndMutableUpdate();
		
		citizens.Add( citizen.gameObject );
	}
}
