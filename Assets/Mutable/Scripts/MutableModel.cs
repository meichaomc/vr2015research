/*
 * ------------------------------------------------------------------------------------------------
 * (c) Anticto Estudi Binari S.L. This file is subject to the terms and conditions defined in 
 * the file 'license.txt', which is part of this package.
 * ------------------------------------------------------------------------------------------------
 */

using UnityEngine; 

using System.IO;
using System.Runtime.InteropServices;
using System.Collections.Generic;


//-------------------------------------------------------------------------------------------------
// This class represents a Mutable Model that can be used to create several Mutable instances to
// be used in Mutable Objects.
//-------------------------------------------------------------------------------------------------
[AddComponentMenu("Mutable/Mutable Model")]
public class MutableModel : MonoBehaviour
{
	public TextAsset MutableModelAsset = null;
	public string ModelDataStreamingFolder;
	public string ModelDataStreamingSuffix = ".mutable_data";

	public GameObject ReferenceObject = null;
	private GameObject m_lastReferenceObject = null;
	
	// This data is extracted from the ReferenceObject at start time
	[HideInInspector]
	public string[] m_referenceObjectBoneNames;

	[HideInInspector]
	public Matrix4x4[] m_referenceObjectBindPoses;
	
	public float ImportScaleFactor = 0.01f;
	
	[HideInInspector]
	public int m_handle = -1;

	[HideInInspector]
	public int m_references = 0;
	
	[HideInInspector]
	public string m_streamingPrefix;
	
	
	//---------------------------------------------------------------------------------------------
	// Immutable Model information cached from the model data
	//---------------------------------------------------------------------------------------------

	// State descriptions
	public struct STATE
	{
		public string name;
		public int[] runtimeParameters;
	};
	[HideInInspector]
	public STATE[] states;
		
	//! Parameter descriptions
	public struct PARAMETER_DESC
	{
		public string name;
		public Mutable.PARAMETER_TYPE type;
		public string[] intOptions;
		public int[] intValues;
	};
	
	[HideInInspector]
	public PARAMETER_DESC[] parameterDescriptions;
	
	//! Default parameter values
	[HideInInspector]
	public MutableParameters defaultParameterValues;
	
	
	public MutableParameters GetDefaultParameterValues()
	{
		InternalInitialize();
		return defaultParameterValues;
	}
	
	
	//! 
	public void Start()
	{
		InternalInitialize();		
	}
	
	
	//! 
	private void InternalInitialize()
	{
		MutableManager manager = GetManager();		
		if (parameterDescriptions==null)
		{
			if (manager)
			{
				manager.InitializeModel( this );
			}
		}
		
			
		if (ReferenceObject!=m_lastReferenceObject)
		{
			m_lastReferenceObject = ReferenceObject;
			m_referenceObjectBoneNames = null;
			m_referenceObjectBindPoses = null;
			if (ReferenceObject)
			{
				SkinnedMeshRenderer referenceMeshRenderer = null;
				if ( ReferenceObject )
				{
					referenceMeshRenderer = ReferenceObject.GetComponentInChildren<SkinnedMeshRenderer>();
				}
				
				if (referenceMeshRenderer!=null)
				{				
					Mesh oldMesh = referenceMeshRenderer.sharedMesh;
					
					int boneCount = referenceMeshRenderer.bones.Length;
					m_referenceObjectBoneNames = new string[ boneCount ];
					m_referenceObjectBindPoses = new Matrix4x4[ boneCount ];
					for (int b=0; b<boneCount; ++b)
					{
						m_referenceObjectBoneNames[b] = referenceMeshRenderer.bones[b].name;
						m_referenceObjectBindPoses[b] = oldMesh.bindposes[b];
					}
				}
			}
		}
	}
	
	
	public void SetReferenceObject( GameObject o )
	{
		ReferenceObject = o;
		InternalInitialize();
	}
	
	
	//! 
	public void FreeUnused()
	{
		if (m_references==0 && m_handle>=0)
		{
			MutableManager manager = GetManager();
			if (manager)
			{
				manager.ReleaseModel( this );
			}
		}
	}
		
	
	//!
	void OnDestroy()
	{
		FreeUnused();
	}
	

	//!
	public MutableManager GetManager()
	{
		if (transform && transform.parent)
		{
			return transform.parent.GetComponent<MutableManager>();
		}
		
		return null;
	}
	
	


    // Get the number of model states available in this object
    public int GetStateCount()
	{
		if (states!=null)
		{
			return states.Length;
		}
		return 0;
	}


    // Get the names of the model states in this object
    public string[] GetStateNames()
	{
		if (states!=null)
		{
			int stateCount = states.Length;
			string[] names = new string[ stateCount ];
			for (int s=0; s<stateCount; ++s)
			{
				names[s] = states[s].name;
			}
			return names;
		}
		
		return new string[0];
	}


    // Get the index of a state from its name. Return -1 if not found.
    public int FindState( string name )
	{
		int result = -1;

		if (states!=null)
		{
			int stateCount = states.Length;
			for (int s=0; s<stateCount; ++s)
			{
				if (states[s].name==name)
				{
					result = s;
				}
			}
		}
		
		return result;
	}

	
	
	
    // Get the number of parameters that are exposed in the provided model state. 
	public int GetStateParameterCount( int state )
	{
		if (states!=null)
		{
			return states[state].runtimeParameters.Length;
		}
		
		return 0;
	}


    // Get the index of one of parameters that are exposed in the provided model state. 
    // "param" goes from 0 to GetStateParameterCount(state)-1
    // The returned index refers to the list of all parameters in the model, so it wil have a
    // value between 0 and GetParameterCount().
    public int GetStateParameterIndex(int state, int param)
	{
		if (states!=null)
		{
			return states[state].runtimeParameters[param];
		}
		return 0;
	}

	// Get the total number of parameters in the model.
	public int GetParameterCount()
	{
		if (parameterDescriptions!=null)
		{
			return parameterDescriptions.Length;
		}
		return 0;
	}
	
	// Get the name of a parameter of the model.
	public string GetParameterName( int p )
	{
		if (parameterDescriptions!=null)
		{
			return parameterDescriptions[p].name;
		}
		return "";
	}
	
	// Get the type of a parameter in the model
	public Mutable.PARAMETER_TYPE GetParameterType( int p )
	{
		if (parameterDescriptions!=null)
		{
			return parameterDescriptions[p].type;
		}
		return Mutable.PARAMETER_TYPE.T_NONE;
	}
	
}


