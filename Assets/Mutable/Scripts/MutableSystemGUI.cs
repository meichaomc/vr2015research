using UnityEngine;
using System.Collections;

[AddComponentMenu("Mutable/System UI")]
public class MutableSystemGUI : MonoBehaviour 
{
			
	public Vector2 Position = new Vector2( 10, 64 );
	
	public enum PositionRelativeTo
    {
        TopLeft,
        TopRight,
		BottomLeft,
		BottomRight
    };
	
	public PositionRelativeTo positionRelativeTo = PositionRelativeTo.TopRight;
	
	public MutableManager mutableManager;
	
	public float updatesPerSecond = 10.0f;
	
	private float accumulatedTime = 0.0f;
	
	private struct SAMPLE
	{
		public int modelCount;
		public int modelBytes;
		public int liveInstanceCount;
		public int cacheBytes;
		public int streamedBytes;
	}
	
	// This will rotate to keep the last samples
	private SAMPLE[] samples;
	private int maxSamples = 512;
	private int sampleCount = 0;
	
	// Use this for initialization
	void Start () 
	{
		samples = new SAMPLE[ maxSamples ];
	}
			
	
	void Update() 
	{			
		float delta = Time.deltaTime;
		accumulatedTime += delta;
		
		if (accumulatedTime>1.0f/updatesPerSecond)
		{
			// Get a new sample of the data
			// \TODO: This is not thread safe, but reading this data is not critical and cannot create
			// threading problems (just some corrupted values sometimes). Fix later.
			SAMPLE newSample = new SAMPLE();			
			newSample.modelCount = Mutable.mutable_get_profile_metric( Mutable.PROFILE_METRIC.PM_MODEL_COUNT );
			newSample.modelBytes = Mutable.mutable_get_profile_metric( Mutable.PROFILE_METRIC.PM_MODEL_BYTES );
			newSample.streamedBytes = Mutable.mutable_get_profile_metric( Mutable.PROFILE_METRIC.PM_STREAMED_BYTES );
			newSample.cacheBytes = Mutable.mutable_get_profile_metric( Mutable.PROFILE_METRIC.PM_CACHE_BYTES );
			newSample.liveInstanceCount = Mutable.mutable_get_profile_metric( Mutable.PROFILE_METRIC.PM_INSTANCE_COUNT );		
			
			// Store the new sample.
			int samplePos = sampleCount % maxSamples;
			samples[samplePos] = newSample;
			sampleCount++;
			
			accumulatedTime -= delta;
		}
	}
			
	
	void OnGUI () 
	{					
		
		// Calculate necessary box size
		int numMetrics = 6;
		int totalY = 10+20*numMetrics;
		int totalX = 300;
		
		Rect rect = new Rect();
		
		switch (positionRelativeTo)
		{
		case PositionRelativeTo.TopRight:
		case PositionRelativeTo.BottomRight:
			rect.xMin = Screen.width-Position.x-totalX;
			rect.xMax = Screen.width-Position.x;
			break;
			
		case PositionRelativeTo.TopLeft:
		case PositionRelativeTo.BottomLeft:
			rect.xMin = Position.x;
			rect.xMax = Position.x+totalX;
			break;
		}
		
		switch (positionRelativeTo)
		{
		case PositionRelativeTo.TopRight:
		case PositionRelativeTo.TopLeft:
			rect.yMin = Position.y;
			rect.yMax = Position.y+totalY;
			break;
			
		case PositionRelativeTo.BottomRight:
		case PositionRelativeTo.BottomLeft:
			rect.yMin = Screen.height-Position.y-totalY;
			rect.yMax = Screen.height-Position.y;
			break;
		}
			
		// Make a background box	
		GUI.Box(rect, "");
		
		if ( mutableManager==null )
		{
			GUI.Box(rect, "");
			GUI.Label(new Rect(rect.xMin+5,rect.yMin+5,290,50), 
				"System UI: No Mutable Manager set.");
		}
		else if (sampleCount>0)
		{		
			int x = (int)rect.xMin+5;
			int y = (int)rect.yMin+5;
			int sizeX = (int)(rect.xMax-rect.xMin);
			
			int sample = (sampleCount-1)%maxSamples;
			
			{
				int v = samples[sample].modelCount;
				GUI.Label( new Rect( x, y, sizeX/2-5, 20 ), "Model #");
				GUI.Label( new Rect( x+sizeX/2, y, sizeX/2-5, 20 ), v.ToString() );
				y += 20;
			}
			
			{
				int v = samples[sample].modelBytes;
				GUI.Label( new Rect( x, y, sizeX/2-5, 20 ), "Model Kb");
				GUI.Label( new Rect( x+sizeX/2, y, sizeX/2-5, 20 ), (v/1024).ToString() );
				y += 20;
			}
			
			{
				int v = samples[sample].streamedBytes;
				GUI.Label( new Rect( x, y, sizeX/2-5, 30 ), "Streamed Kb");
				GUI.Label( new Rect( x+sizeX/2, y, sizeX/2-5, 20 ), (v/1024).ToString() );
				y += 20;
			}
			
			{
				int v = samples[sample].cacheBytes;
				GUI.Label( new Rect( x, y, sizeX/2-5, 20 ), "Cache Kb");
				GUI.Label( new Rect( x+sizeX/2, y, sizeX/2-5, 20 ), (v/1024).ToString() );
				y += 20;
			}
			
			{
				int v = samples[sample].liveInstanceCount;
				GUI.Label( new Rect( x, y, sizeX/2-5, 20 ), "Live Instance #");
				GUI.Label( new Rect( x+sizeX/2, y, sizeX/2-5, 20 ), v.ToString() );
				y += 20;
			}			
			
		}
		
	}
	
}
