using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;


[CustomEditor(typeof(MutableModel))] 
public class MutableModelEditor : Editor 
{
	
	//private int divisionsX = 8;
	//private int divisionsY = 8;
	
	
	public static T[] GetAtPath<T> (string path) 
	{
	    ArrayList al = new ArrayList();
	    string [] fileEntries = Directory.GetFiles(Application.dataPath+"/"+path);
	
	    foreach(string fileName in fileEntries)
	    {
			int index = Mathf.Max( fileName.LastIndexOf("/"), fileName.LastIndexOf("\\") );
	        string localPath = "Assets/" + path;
	        if (index > 0)
	            localPath += fileName.Substring(index);
	        Object t = AssetDatabase.LoadAssetAtPath(localPath, typeof(T));
	        if(t != null)
	            al.Add(t);
	    }
	
	    T[] result = new T[al.Count];
	    for(int i=0;i<al.Count;i++)
	        result[i] = (T)al[i];
		
	    return result;
	}
	
	
    public override void OnInspectorGUI() 
	{
		DrawDefaultInspector ();
		
		//MutableModel myTarget = (MutableModel) target;

        //divisionsX = EditorGUILayout.IntSlider("Grid X", divisionsX, 1, 16);		
        //divisionsY = EditorGUILayout.IntSlider("Grid Y", divisionsY, 1, 16);		
        
		/*
		if (GUILayout.Button("Scan data files"))
		{
			// Look for all the similarly named assets around the model text asset.
			TextAsset modelAsset = myTarget.MutableModelAsset;
			
			string modelPath = AssetDatabase.GetAssetPath( modelAsset );
			string modelFolder = Path.GetDirectoryName(modelPath);
			
			// Skip "Assets/"
			modelFolder = modelFolder.Substring( 7 );
						
			TextAsset[] assetsInThePath = GetAtPath<TextAsset>( modelFolder );
			
			List<TextAsset> found = new List<TextAsset>();
			
			// Get whatever is before the point (if any) in the asset name
			string modelName = Path.GetFileNameWithoutExtension( modelAsset.name );
			foreach( var text in assetsInThePath )
			{
				if ( text.name.StartsWith( modelName ) && text!=modelAsset )
				{
					found.Add( text );
				}
			}
			
			myTarget.MutableModelDataAssets = found.ToArray();			
			
			EditorUtility.SetDirty(myTarget);
		}
		*/
		
		if (GUI.changed)
		{
			EditorUtility.SetDirty(target);
		}		
    }
	
}
