﻿using UnityEngine;
using System.Collections;

public class changeHair : MonoBehaviour {
	public string hairtag;
	private fakeCircleLinkList hairs;
	//public GameObject virtualHuamn;


	// Use this for initialization
	void Start () {
		hairs = new fakeCircleLinkList();
		GameObject [] hairsmeshes = GameObject.FindGameObjectsWithTag(hairtag);
		foreach(GameObject go in hairsmeshes)
		{

			hairs.put(go.GetComponent<MeshRenderer>());
		}
	}

	// Update is called once per frame
	void Update () {

	}

	public void nextHair()
	{
		MeshRenderer skinnedmeshr;
		skinnedmeshr = hairs.getCurrent() as MeshRenderer;
		if(skinnedmeshr!=null){
			skinnedmeshr.enabled = false;
		}
		skinnedmeshr = hairs.getNext() as MeshRenderer;
		skinnedmeshr.enabled = true;
	}

	public void lastHair()
	{
		MeshRenderer skinnedmeshr;
		skinnedmeshr = hairs.getCurrent() as MeshRenderer;
		if(skinnedmeshr!=null){
			skinnedmeshr.enabled = false;
		}
		skinnedmeshr = hairs.getLast() as MeshRenderer;
		skinnedmeshr.enabled = true;
	}



}

public class fakeCircleLinkList
{
	private ArrayList container;
	private int index = 0;
	private object current;
	public fakeCircleLinkList()
	{
		container = new ArrayList();

	}

	public void put(object o)
	{
		container.Add(o);
	}

	public object getNext()
	{
		if(index<container.Count-1)
		{
			current = container[++index];
			return current;

		}else
		{
			index = -1;
			current = container[++index];
			return current;
		}


	}

	public object getCurrent()
	{
		return current;
	}

	public object getLast()
	{
		if(index>0)
		{
			current = container[--index];
			return current;
			
		}else
		{
			index = container.Count;
			current = container[--index];
			return current;
		}
		
		
	}


}




	
	

