﻿using UnityEngine;
using System.Collections;

public class startButton : MonoBehaviour {
	public GameObject cameraScene;
	bool finish = false;
	int flag = 0;
	// Use this for initialization
	void Start () {
		cameraScene.transform.Translate (new Vector3(0,-100,0));
	}
	
	// Update is called once per frame
	void Update () {
		if (finish) 
		{
			cameraScene.transform.position+=new Vector3(0,1f,0);
			flag++;
			if (flag==100){
				finish=false;
				closeGUI();
			}
		}
	}
	void OnGUI () {
		// Make a background box
		GUI.Box(new Rect(Screen.width-100,Screen.height-90,100,90), "");
		
		// Make the first button. If it is pressed, Application.Loadlevel (1) will be executed
		if(GUI.Button(new Rect(Screen.width-90,Screen.height-60,80,20), "Finish")) {
			finish=true;
		}
		
		// Make the second button.

		}

	void closeGUI()
	{
		GameObject.Find ("MutableManager").SetActive(false);
		gameObject.SetActive(false);
	}
}
