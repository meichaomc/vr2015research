Shader "LightMapped/Cutout" 
{
   Properties 
   {
      _Color ("Main Color", Color) = (1,1,1,1)
      _MainTex ("Base (RGBA)", 2D) = "white" {}
      _LightMap ("Lightmap (RGB)", 2D) = "gray" {}
   }
   
   Category 
   {
	   Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
	   ZWrite On
	   ZTest LEqual
	   Alphatest Greater 0.3
	   //Blend SrcAlpha OneMinusSrcAlpha
   
	   SubShader 
	   {
	   	   	// Ambient pass
			Pass 
			{
				Name "BASE"
				Tags {"LightMode" = "Always" /* Upgrade NOTE: changed from PixelOrNone to Always */}
				Color [_PPLAmbient]
				
				BindChannels 
				{
					Bind "Vertex", vertex
					Bind "normal", normal
					Bind "texcoord1", texcoord0 // lightmap uses 2nd uv
					Bind "texcoord", texcoord1 // main uses 1st uv
				}
				
				SetTexture [_LightMap] 
				{
					constantColor [_Color]
					combine texture * constant
				}
				
				SetTexture [_MainTex] 
				{
					constantColor [_Color]
					combine texture * previous, texture * constant
				}
			}	 
	   }
   
	   FallBack "VertexLit", 2
	} 
}