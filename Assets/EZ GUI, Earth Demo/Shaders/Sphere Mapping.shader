Shader "Reflective/VertexLit Spheremap" {
Properties {
    _Color ("Main Color", Color) = (1,1,1,1)
    _SpecColor ("Spec Color", Color) = (1,1,1,1)
    _Shininess ("Shininess", Range (0.03, 1)) = 0.7
    _MainTex ("Base (RGB) RefStrength (A)", 2D) = "white" {}
    _Spheremap ("Reflection Spheremap", 2D) = "white" { TexGen SphereMap }
}

Category {
    Tags { "RenderType"="Opaque" }
    /* Upgrade NOTE: commented out, possibly part of old style per-pixel lighting: Blend AppSrcAdd AppDstAdd */
    Fog { Color [_AddFog] }

    SubShader {
        Pass {
            Name "BASE"
            Tags {"LightMode" = "Always"}
            Material {
                Diffuse [_Color]
                Ambient (1,1,1,1)
                Shininess [_Shininess]
                Specular [_SpecColor]
            }
            Lighting On
            SeparateSpecular on
            SetTexture [_MainTex] {
                combine texture * primary DOUBLE,
                    texture * primary
            }
            SetTexture [_Spheremap] {
                combine texture * previous alpha + previous,
                    previous
            }
        }
    }
}
FallBack "VertexLit"
} 