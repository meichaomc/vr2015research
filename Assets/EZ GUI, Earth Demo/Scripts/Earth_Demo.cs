using UnityEngine;
using System.Collections;

public class Earth_Demo : MonoBehaviour 
{
	public ParticleEmitter groundCloud;
	public GameObject missilePrefab;
	public GameObject missilePath;
	public AudioSource missileSnd;
	
	public void FireMissile()
	{
		GameObject missile = (GameObject) Instantiate(missilePrefab, missilePath.transform.position, Quaternion.identity);
		Destroy(missile, 25f);
		
		groundCloud.Emit();
		missileSnd.Play();
	}
}
