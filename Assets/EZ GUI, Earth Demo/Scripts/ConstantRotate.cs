using UnityEngine;
using System.Collections;

public class ConstantRotate : MonoBehaviour 
{
	float rot = 30;

	// Update is called once per frame
	void Update () 
	{
		rot -= 1 * Time.deltaTime;
		transform.localEulerAngles = new Vector3(0,rot,0);
	}
}
