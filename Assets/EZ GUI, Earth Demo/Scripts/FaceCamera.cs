using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class FaceCamera : MonoBehaviour 
{
	// Update is called once per frame
	void Update () 
	{
		transform.LookAt(Camera.main.transform.position);
	}
}
