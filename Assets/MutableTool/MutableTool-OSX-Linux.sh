#!/bin/bash

MACHINE_TYPE=`uname -m`

if [ "$(uname)" == "Darwin" ]; then
    if [ ${MACHINE_TYPE} == 'x86_64' ]; then
       ./OSX-x86_64/MutableTool
    else
        echo "32-bit OSX is not implemented in this launch script."
    fi

elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
    # Do something under Linux platform
    echo "Platform not implemented in this launch script."

elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW32_NT" ]; then
    # Do something under Windows NT platform
    echo "Platform not implemented in this launch script."

fi

