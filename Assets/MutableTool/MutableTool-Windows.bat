echo off

rem Select the 32 or 64 bit version of the application

SETLOCAL ENABLEEXTENSIONS
IF DEFINED PROGRAMFILES(X86)  (
    SET app=Win-x86_64\MutableTool.exe 
) ELSE (
    SET app=Win-x86_32\MutableTool.exe 
)

START %app%