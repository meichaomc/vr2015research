﻿using UnityEngine;
using System.Collections;

public class SychronizeMutableGuis : MonoBehaviour {

	// Use this for initialization
	public GameObject [] objectsToSyn;
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		foreach(GameObject o in objectsToSyn)
		{
			o.GetComponent<MutableInstance>().parameters = gameObject.GetComponent<MutableInstance>().parameters.Clone();
			o.GetComponent<MutableInstance>().MutableUpdate(false);
		}

	}
}
